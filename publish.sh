export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain fasttrack --domain-owner 680962620693 --query authorizationToken --output text --profile codeartifact`


poetry config repositories.codeartifact https://fasttrack-680962620693.d.codeartifact.us-east-1.amazonaws.com/pypi/fasttrack/
poetry config http-basic.codeartifact aws $CODEARTIFACT_AUTH_TOKEN
#
#
poetry publish --build -r codeartifact
