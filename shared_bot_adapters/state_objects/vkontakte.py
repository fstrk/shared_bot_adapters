import json
from typing import List, Optional

import attr


from .common import (
    AbstractButton,
    AbstractCard,
    AbstractMessage,
    AbstractQRButtonGroup,
    AbstractStateObject,
    clean_slashR_and_tabs,
    field,
)
from .converters import convert_to_less_than_20s, type_or_none
from .telegram import _convert_to_list_of_lists
from .validators import gte, is_instance_of, list_of, lte, one_of


@attr.s(auto_attribs=True)
class VKBaseMessage(AbstractMessage):
    """
    Новая версия базового сообщения ВК.
    В отличие от старой, когда был только VKMessage на всё,
    от этой новой версии наследуются VKImageMessage, VKAudioMessage и тд
    """


@attr.s(auto_attribs=True)
class VKMessage(VKBaseMessage):
    """
    Сообщение с текстом и медиавложениями. Может содержать множество медиавложений.
    Может содержать:
        - контент (текст)
        - медиавложения (изображение, локацию)
    Должно содержать хотя бы одно из выше перечисленного
    """

    text: str = field(default="", converter=str, renderable=True, localizable=True)
    attachments: List = field(factory=list)

    def render(self, renderer):
        """
        Рендерим attachments отдельно, так как это массив словарей
        и методом родителя они не рендерятся
        """

        from apps.core.utils import guess_message_type

        super().render(renderer=renderer)
        for attachment in self.attachments:
            attachment["url"] = renderer(attachment["url"])
            # после рендера тип вложения мог измениться. Выясняем его снова
            attachment["type"] = guess_message_type(self.platform, attachment["url"])

    @attachments.validator
    def max_10(self, _, value):
        if len(value) > 10:
            self.raise_(f"Cannot have > 10 attachments (now {len(value)})")

    def api_dict(self):
        return {"message": self.text}

    def empty(self):
        return not self.text and len(self.attachments) == 0

    def post_render_clean(self):
        """
        Почистить контент после рендера
        """
        self.text = clean_slashR_and_tabs(self.text)
        self.text = self.text[:4096]

    def has_only_text(self):
        return not self.empty() and len(self.attachments) == 0

    def repr_for_report(self):
        return self.text


@attr.s(auto_attribs=True)
class VKTextMessage(VKBaseMessage):
    """
    Сообщение с текстом
    """

    text: str = field(default="", converter=str, renderable=True, localizable=True)

    def rebuild_validate(self):
        """
        Приводим старый стиль к новому: достаем из стартого атрибута content
        атрибут text
        """
        if hasattr(self, "content"):
            self.text = self.content.get("text", "")
            del self.content
        super().rebuild_validate()

    def post_render_clean(self):
        """
        Почистить контент после рендера
        """
        self.text = clean_slashR_and_tabs(self.text)
        self.text = self.text[:4096]

    def api_dict(self):
        return attr.asdict(self)

    def empty(self):
        return not self.text

    def repr_for_report(self):
        return self.text


@attr.s(auto_attribs=True)
class VKImageMessage(VKBaseMessage):
    """
    Сообщение с текстом. Содержит атрибут: url, caption
    """

    url: str = field(default="", converter=str, renderable=True)
    caption: Optional[str] = field(
        default=None, converter=type_or_none(str), renderable=True, localizable=True
    )

    def api_dict(self):
        return attr.asdict(self)

    def empty(self):
        return not self.url

    def repr_for_report(self):
        return self.url


@attr.s
class VKAudioMessage(VKImageMessage):
    """
    Сообщение с аудио. Содержит атрибут: url, caption
    """


@attr.s
class VKVideoMessage(VKImageMessage):
    """
    Сообщение с видео. Содержит атрибут: url, caption
    """


@attr.s
class VKDocumentMessage(VKImageMessage):
    """
    Сообщение с документами. Содержит атрибут: url, caption
    """


class VKCommandMessage:
    """
    Легаси-класс, который нельзя стирать, потому что в некоторых ботах в JSON
    встречается "py/object": "sending.state_objects.vkontakte.VKCommandMessage"
    """


class VKCommandGroup:
    """
    Легаси-класс, который нельзя стирать, потому что в некоторых ботах в JSON
    встречается  "py/object": "sending.state_objects.vkontakte.VKCommandGroup"
    """


@attr.s
class VKButton(AbstractButton):
    """
    Кнопка в клавиатуре VK.
    """

    allowed_types = [AbstractButton.ACTION, AbstractButton.URL]

    COLOR_BLUE = "blue"
    COLOR_WHITE = "white"
    COLOR_RED = "red"
    COLOR_GREEN = "green"

    color = field(
        default=None,
        validator=one_of([COLOR_BLUE, COLOR_WHITE, COLOR_RED, COLOR_GREEN, None]),
    )

    @color.validator
    def only_for_action_buttons(self, attribute, value):
        if self.type == AbstractButton.URL and value:
            self.raise_("URL buttons cannot have color setting.")

    def api_dict(self) -> dict:
        color_translations = {
            self.COLOR_BLUE: "primary",
            self.COLOR_WHITE: "default",
            self.COLOR_RED: "negative",
            self.COLOR_GREEN: "positive",
            None: "default",
        }
        if self.type == AbstractButton.ACTION:
            data = {
                "action": {
                    "type": "text",
                    "label": self.text,
                    "payload": json.dumps({"command": self.data}, ensure_ascii=False),
                },
                "color": color_translations[self.color],
            }
        else:
            # кнопки-ссылки не поддерживают цвет. 13.01.2020
            data = {
                "action": {"type": "open_link", "label": self.text, "link": self.data}
            }
        return data


@attr.s(auto_attribs=True)
class VKQRButtonGroup(AbstractQRButtonGroup):
    """
    Клавиатура VK. Должна быть списком из списков VKKeyboardButton
    """

    buttons: List[List[VKButton]] = field(
        converter=_convert_to_list_of_lists,
        renderable=True,
        has_buttons=True,
        localizable=True,
    )
    one_time: bool = field(default=True)

    def __bool__(self):
        """
        Объект непустой, если есть хотя бы одна кнопка
        """
        btns = self.buttons
        return bool((isinstance(btns, list) and len(btns) and len(btns[0])))

    @buttons.default
    def buttons_default(self):
        return [[]]

    @buttons.validator
    def must_be_list_of_lists_of_vkbutton(self, _, value):
        """
        Если передано что-то, кроме списка списков VKButton,
        - ломаемся
        """
        if value:
            try:
                for row in value:
                    for button in row:
                        assert isinstance(button, VKButton)
            except (AssertionError, TypeError, IndexError):
                self.raise_(
                    f"Buttons must be list of lists: [[VKButton]]," f"not {value}"
                )

    def api_dict(self) -> dict:
        data = {
            "one_time": self.one_time,
            "buttons": [[button.api_dict() for button in row] for row in self.buttons],
        }
        return data


class VKActions(object):
    def __init__(self, keyboard=None, commands=None):
        self.keyboard = keyboard
        self.commands = commands

    def render(self, renderer):
        if self.commands:
            self.commands.render(renderer)

    @property
    def buttons(self):
        return self.keyboard or self.commands


@attr.s(auto_attribs=True)
class VKSleepMessage(AbstractMessage):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    # TODO: Так как в билдере ещё не понятно как лучше ставить сообщения-паузы
    # этот класс пока не закончен. Думаю лучше создавать сразу два
    # сообщения-паузы, если в билдере строится пауза больше 10 секунд

    # не можем унаследовать этот тип от VKMessage, т.к. в здесь нет текста,
    # а в VKMessage - есть
    seconds: int = field(
        default=1, converter=convert_to_less_than_20s, validator=[gte(1), lte(20)]
    )
    show_typing: bool = field(default=False)

    def render(self, renderer):
        """ Нечего рендерить. """

    def api_dict(self):
        data = {"action": "typing"}
        return data

    def empty(self):
        return False

    def repr_for_report(self):
        return ""

    def execute_command(self):
        json_data = json.dumps(self.api_dict(), ensure_ascii=False)
        return f"API.messages.setActivity({json_data});"


@attr.s(auto_attribs=True)
class VKCard(AbstractCard):
    """
    Сообщение-карточка вконтакте
    """

    title: str = field(
        default="", converter=lambda x: str(x)[:80], renderable=True, localizable=True
    )
    subtitle: str = field(
        default="", converter=lambda x: str(x)[:80], renderable=True, localizable=True
    )
    image_url: str = field(default="", renderable=True)

    # item_url - не используется, но добавлен для совместимости
    # с остальными обработчиками карточек
    item_url: Optional[str] = field(default=None)
    buttons: List[VKButton] = field(
        factory=list,
        validator=list_of(
            VKButton,
            maxlen=3,
            minlen=1,
            readable_error="В карточке некорректное количество кнопок",
        ),
        renderable=True,
        has_buttons=True,
        none_to_default=True,
        localizable=True,
    )

    @subtitle.validator
    def must_be_description_if_title_exists(self, attribute, value):
        if self.title and not value:
            self.raise_("If title is set, then subtitle cannot be empty.")

    @title.validator
    def must_be_title_if_image_url_not_exists(self, attribute, value):
        if not self.image_url and not value:
            self.raise_("If image_url is not set, then title cannot be empty.")

    def api_dict(self) -> dict:
        return {
            "title": self.title,
            "description": self.subtitle,
            "image_url": self.image_url,
            "buttons": [button.api_dict() for button in self.buttons],
        }

    def repr_for_report(self):
        repr = self.title or ""
        if self.subtitle:
            repr += "\n" + self.subtitle
        return repr


@attr.s(auto_attribs=True)
class VKCardGroup(AbstractStateObject):
    """
    Группа карточек вконтакте.
    """

    cards: List[VKCard] = field(
        factory=list,
        renderable=True,
        has_buttons=True,
        has_cards=True,
        localizable=True,
    )

    @cards.validator
    def max_10(self, attribute, value):
        list_of(VKCard, maxlen=10, minlen=1)(self, attribute, value)

    @cards.validator
    def equal_card_content(self, attribute, value):
        """ ограничение ВК: каждая карточка должна иметь одинаковое кол-во кнопок"""
        button_counts = [len(card.buttons) for card in value]

        for count in button_counts[1:]:
            if count != button_counts[0]:
                self.raise_(
                    f"Number of buttons must be "
                    f"the same for all cards (now: {button_counts})",
                    msg_for_frontend=f"Кол-во кнопок должно быть одинаковым во всех "
                    f"карточках. Сейчас количества разные: {button_counts})",
                )

        image_urls = [card.image_url for card in value]
        for image_url in image_urls[1:]:
            if bool(image_url) != bool(image_urls[0]):
                self.raise_(
                    f"Either all cards must have images, or no cards should have images.",
                    msg_for_frontend=f"Изображения должны быть либо у всех карточек, "
                    f"либо ни у одной.",
                )

    def api_dict(self) -> list:
        api_dict = [card.api_dict() for card in self.cards]
        return api_dict


@attr.s(auto_attribs=True)
class VKCarouselMessage(VKBaseMessage):
    """
    Карусель из карточек. Обязательно должно предшествовать текстовое сообщение.
    Содержит до 10 карточек.
    """

    text: str = field(converter=str, renderable=True, localizable=True)

    card_group: Optional[VKCardGroup] = field(
        default=None,
        validator=is_instance_of(VKCardGroup),
        renderable=True,
        has_buttons=True,
        has_cards=True,
        localizable=True,
    )

    def api_dict(self) -> dict:
        if self.card_group:
            return {"type": "carousel", "elements": self.card_group.api_dict()}
        return {}

    def empty(self):
        return not self.card_group.cards

    def repr_for_report(self):
        card_reprs = [card.repr_for_report() for card in self.card_group.cards]
        card_reprs = [r for r in card_reprs if r]
        return "\n".join(card_reprs)
