from collections import Callable, Iterable, Sequence
from typing import Optional, Any


def is_instance_of(type_: Any) -> Callable:
    """Фабрика валидаторов для attrs, которая проверяе соответствие аргумента
    какому-то типу"""

    def validator(self, atribute, value):
        if not isinstance(value, type_):
            self.raise_(
                f"Bad {atribute.name}: exptected type {type_}, " f"got {type(value)}"
            )

    return validator


def one_of(choices: Iterable) -> Callable:
    """
    Фабрика валидаторов, которые передаются в attrs
    и вызывают ошибку через метод класса self.raise_().
    Валидатор прроверяет, что переданный элемент является одним из списка
    choices.
    """

    def validator(self, atribute, value):
        if value not in choices:
            self.raise_(
                f"Bad {atribute.name}: exptected one of {choices}, " f"got {value}"
            )

    return validator


def list_of(
    type_: Any, maxlen: int = 100, minlen: int = 0, readable_error: Optional[str] = None
) -> Callable:
    """
    Фабрика валидаторов, которые передаются в attrs
    и вызывают ошибку через метод класса self.raise_().

    Валидатор прроверяет, что переданный элемент является списком объектов
    типа type_ кол-вом не меньше minlen и не больше maxlen.

    readable_error: если передан - транслируется в raise, чтобы пользователи
    на фронте видели адекватное описание ошибки
    """

    def validator(self, atribute, value):
        try:
            for item in value:
                assert isinstance(item, type_)
        except (AssertionError, TypeError, IndexError):
            self.raise_(
                f"Bad {atribute.name}: expected list of {type_}, " f"got {value}"
            )

        if len(value) < minlen or len(value) > maxlen:

            dev_message = (
                f"Bad {atribute.name}: list of {type_} must be "
                f">= {minlen} and <= {maxlen} (now {len(value)})"
            )
            if not readable_error:
                self.raise_(dev_message)
            else:
                user_message = (
                    f"{readable_error}: должно быть от {minlen} до {maxlen} "
                    f"(сейчас {len(value)})"
                )
                self.raise_(dev_message, user_message)

    return validator


def lte(limit: int, nullable: bool = False) -> Callable:
    """
    Фабрика валидаторов, которые передаются в attrs
    и вызывают ошибку через метод класса self.raise_().

    Валидатор прроверяет, что переданный элемент не больше value.
    """

    def validator(self, atribute, value):
        # если разрешены None-значения
        if nullable and value is None:
            return
        try:
            value = float(value)
        except (TypeError, ValueError):
            self.raise_(f"Bad {atribute.name}: must be a number.")
        if value > limit:
            self.raise_(f"Bad {atribute.name}: must be <= {limit}.")

    return validator


def gte(limit: int, nullable: bool = False) -> Callable:
    """
    Фабрика валидаторов, которые передаются в attrs
    и вызывают ошибку через метод класса self.raise_().

    Валидатор прроверяет, что переданный элемент не меньше value.
    """

    def validator(self, atribute, value):
        # если разрешены None-значения
        if nullable and value is None:
            return
        try:
            value = float(value)
        except (TypeError, ValueError):
            self.raise_(f"Bad {atribute.name}: must be a number.")
        if value < limit:
            self.raise_(f"Bad {atribute.name}: must be >= {limit}.")

    return validator
