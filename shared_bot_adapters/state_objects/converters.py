from collections import Callable
from typing import Any


def convert_to_less_than_20s(value):
    """Преобразовать значение, большее 20, в 20 (для секунд SleepMessage)"""
    if value > 20:
        return 20
    return value


def type_or_none(type_: Any) -> Callable:
    """
    Фабрика конвертеров для attrs, которые пытаются привести значение
    к определенному типу и возвращают None в случае неудачи
    :param type_:
    :return:
    """

    def converter(value):
        if value is None:
            return
        try:
            return type_(value)
        except (ValueError, TypeError):
            return

    return converter
