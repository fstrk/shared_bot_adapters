import logging
from typing import List, Optional, Sequence
from uuid import UUID, uuid4

import attr

from ..exceptions import StateObjectError
from .common import AbstractMessage, AbstractQRButtonGroup, AbstractStateObject, field
from .validators import is_instance_of, list_of, one_of

logger = logging.getLogger(__name__)


def _list_or_none(messages):
    # на всякий случай, если передан не список, превращаем его в список
    # (P.S. Где это нужно вообще?)
    if messages is None:
        return []
    if messages and not isinstance(messages, list):
        messages = [messages]
    return messages


@attr.s(auto_attribs=True)
class BotState(AbstractStateObject):
    """
    Объект состояния чата.
    Содержит список сообщений, которые нужно отправить юзеру.
    и несколько общих атрибутов:

    - qr_buttons: кнопки быстрого ответа, которые рендерятся вместо клавиатуры
    - answer_callback_query: объект AnswerCallbackQuery. Если он есть,
                то сендер выполнит ответ на callbackquery
    - kill_previous_markups: убивать ли инлайн-кнопки
        всех предыдущих сообщений (для Telegram)
    - extra_context: дополнительные контекстные переменные для рендера.
        Обычно это локальные переменные типа результатов ответов на вопросы,
        которые динамически формируются какой-то из вьюх.
    - system_initiated: инициируется ли стейт системой
    """

    messages: Sequence[AbstractMessage] = field(
        factory=list,
        converter=_list_or_none,
        renderable=True,
        has_buttons=True,
        has_cards=True,
        localizable=True,
        # 100 - цифра с потолка, просто потому что это "достаточного много"
        validator=list_of(AbstractMessage, maxlen=100),
    )

    qr_buttons: Optional[AbstractQRButtonGroup] = field(
        default=None, renderable=True, has_buttons=True, localizable=True
    )

    extra_context: dict = field(
        factory=dict, converter=lambda val: val or {}, repr=False
    )

    # Уникальный идентификатор ботстейта.
    uuid: UUID = field(factory=uuid4)

    # Дополнительные данные, передающиейся вместе со стейтом.
    meta: dict = field(factory=dict, converter=lambda val: val or {})

    def empty(self) -> bool:
        """
        пустой ли стейт?
        если нет ни одного непустого сообщения,
        нет QR-кнопок и текстовых команд - то пустой
        """
        messages = [m for m in self.messages if not m.empty()]
        return not messages and not self.qr_buttons

    def cancel(self) -> None:
        self.cancelled = True

    @qr_buttons.validator
    def _validate_qr_buttons(self, attribute, value):
        if value is None:
            return
        if not isinstance(value, AbstractQRButtonGroup):
            self.raise_(
                f"qr_buttons must be one of " f"QRButtonGroup type, not {type(value)}"
            )

    def prepend_messages(self, messages: Sequence[AbstractMessage]) -> None:
        """Присоединить к стейту сообщения в самое начало списка сообщений"""
        if not isinstance(messages, list):
            raise StateObjectError("messages must be of list type")
        # mypy ругается на сумму Sequence и List
        self.messages = messages + self.messages  # type: ignore

    def append_messages(self, messages: Sequence[AbstractMessage]) -> None:
        """Присоединить к стейту сообщения в самый конец списка сообщений"""
        if not isinstance(messages, list):
            raise StateObjectError("messages must be of list type")

        # mypy ругается на сумму Sequence и List
        self.messages = self.messages + messages  # type: ignore

    def update_qr_buttons(self, qr_buttons):
        """
        Обновить QR-кнопки стейта на новые кнопки.
        Кнопки обновляются, только если новые кнопки не равны None.
        Иначе, если у добавляемого стейта нет QR-кнопок, а у старого стейта
        они есть, то они просто пропадут.
        """
        if not qr_buttons:
            return
        self.qr_buttons = qr_buttons
