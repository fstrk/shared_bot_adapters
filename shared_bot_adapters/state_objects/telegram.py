from typing import List, Optional, Union, Any
from urllib.parse import urlparse

import attr


from .common import (
    AbstractButton,
    AbstractMessage,
    AbstractQRButtonGroup,
    AbstractStateObject,
    clean_slashR_and_tabs,
    field,
)
from .converters import convert_to_less_than_20s, type_or_none
from .validators import gte, lte


def _convert_to_list_of_lists(value):
    """
    Приводим пустой список к списку списков
    """
    if value == [] or value is None:
        return [[]]
    return value


def shorten_caption(value):
    """Привести и укоротить капшен под стандарт телеграмма"""
    if value is None:
        return ""
    return str(value)[:1024]


@attr.s(auto_attribs=True)
class TGInlineButton(AbstractButton):
    """
    Инлайн кнопка Telegram
    """

    shorten_url: bool = field(default=True)
    allowed_types = [
        AbstractButton.ACTION,
        AbstractButton.URL,
        AbstractButton.SHARE_BOT,
    ]

    def api_dict(self):
        data = {"text": self.text}
        if self.type == self.ACTION:
            data.update({"callback_data": self.data})
        if self.type == self.SHARE_BOT:
            data.update({"switch_inline_query": self.data})
        if self.type == self.URL:
            data.update({"url": self.data})
        return data

    def validate_data(self, attr, val):
        """ Для типа SHARE_BOT необязателен атрибут data """
        if self.type == self.SHARE_BOT:
            return
        else:
            super().validate_data(attr, val)

    def can_be_shorten(self) -> bool:
        """
        Можно ли укорачивать такие ссылки.
        Не укорачиваем ссылки, ведущие на телеграм страницы и диплинки,
        чтобы пользователя не выкидывало из бота.
        """
        if not self.data:
            return False

        url = urlparse(self.data)
        if url.scheme == "tg" or url.netloc in ("t.me", "telegram.org"):
            return False

        return True


@attr.s(auto_attribs=True)
class TGInlineButtonGroup(AbstractStateObject):
    """
    Инлайн группа кнопок Telegram. Должна быть списком списков.
    """

    buttons: List[List[TGInlineButton]] = field(
        renderable=True,
        localizable=True,
        default=None,
        converter=_convert_to_list_of_lists,
        has_buttons=True,
    )

    @buttons.validator
    def must_be_list_of_lists_of_tginlinebutton(self, _, value):
        """
        Если передано что-то, кроме списка списков TGInlineButton,
        - ломаемся
        """
        if value:
            try:
                for row in value:
                    for button in row:
                        assert isinstance(button, TGInlineButton)
            except (AssertionError, TypeError, IndexError):
                self.raise_(
                    f"Buttons must be list of lists: [[TGInlineButton]]," f"not {value}"
                )

    def __repr__(self):
        return ", ".join(str(button) for row in self.buttons for button in row)

    def api_dict(self):
        data = [[button.api_dict() for button in row] for row in self.buttons]
        return data


@attr.s(auto_attribs=True)
class TGMessage(AbstractMessage):
    inline_buttons: Optional[TGInlineButtonGroup] = field(
        default=None, renderable=True, has_buttons=True, localizable=True
    )
    replace_telegram_msg: bool = field(default=None)
    restore_qwerty_keyboard: bool = field(default=False)

    def class_name(self):
        """
        используется в HTML-шаблонах визуального редактора,
        чтобы различать представление разных типов сообщений
        """
        return self.__class__.__name__

    @inline_buttons.validator
    def one_of_two_attrs(self, *a, **kw):
        if self.inline_buttons and self.restore_qwerty_keyboard:
            self.raise_(
                'A message cannot have "inline buttons" '
                'and "restore qwerty keyboard" at the same time.'
            )

    @inline_buttons.validator
    def none_or_group(self, _, value):
        if value is None:
            return
        if not isinstance(value, TGInlineButtonGroup):
            self.raise_(
                f"inline_buttons must be None or TGInlineButtonGroup, "
                f"not {type(value)}"
            )


@attr.s(auto_attribs=True)
class TGTextMessage(TGMessage):
    """СОобщение с текстовым контентом
    Должно содержать:
        - контент (текст)
        - formatted: использовать ли форматирование Markdown
            для отображения жирного шрифта/курсива/пр."""

    text: str = field(default="", converter=str, renderable=True, localizable=True)
    formatted: bool = field(default=True, converter=bool)
    web_preview: bool = field(default=True, converter=bool)

    def empty(self):
        return not self.text

    def repr_for_report(self):
        return self.text

    def post_render_clean(self):
        """
        Почистить контент после рендера
        """
        self.text = clean_slashR_and_tabs(self.text)
        self.text = self.text[:4000]

    def rebuild_validate(self):
        """
        Приводим старый стиль к новому: достаем из стартого атрибута content
        атрибут text
        """
        if hasattr(self, "content"):
            self.text = self.content.get("text", "")
            del self.content
        super().rebuild_validate()

    def api_dict(self):
        ib = self.inline_buttons
        data = {
            "message": {"text": self.text},
            "inline_buttons": ib.api_dict() if ib else None,
        }
        return data


@attr.s(auto_attribs=True)
class TGImageMessage(TGMessage):
    """Сообщение с изображением
    Должно содержать:
        - контент (картинку)
        - (опционально) ID сообщения, которое нужно заменить
                        этим новым сообщением
        - (опционально) инлайн-кнопки (список списков).
    """

    url: str = field(default="", renderable=True)
    caption: str = field(
        default="", converter=shorten_caption, renderable=True, localizable=True
    )

    def rebuild_validate(self):
        """
        Приводим старый стиль к новому: достаем из стартого атрибута content
        атрибуты url и caption
        """
        if hasattr(self, "content"):
            self.url = self.content.get("url", "")
            self.caption = self.content.get("caption", "")
            del self.content
        super().rebuild_validate()

    def empty(self):
        return not self.url

    def repr_for_report(self):
        return self.url

    def api_dict(self):
        ib = self.inline_buttons
        data = {
            "message": {"url": self.url, "caption": self.caption},
            "inline_buttons": ib.api_dict() if ib else None,
        }
        return data

    def post_render_clean(self):
        """
        Почистить контент после рендера
        """
        self.caption = shorten_caption(self.caption)


@attr.s
class TGAudioMessage(TGImageMessage):
    """Сообщение с аудио"""


@attr.s
class TGDocumentMessage(TGImageMessage):
    """Сообщение с файлом"""

    def change_class(self, cls: Any) -> None:
        """ Сменить класс (с документа на изображение/аудио/и тд"""
        self.__class__ = cls


@attr.s(auto_attribs=True)
class TGLocationMessage(TGMessage):
    """Сообщение с локацией """

    def coercible_to_float(self, attribute, value):
        """
        Валидатор, который проверяет, можно ли преобразовать
        значение во float"""
        try:
            float(value)
        except (ValueError, TypeError):
            self.raise_(f"{attribute.name} must be float")

    latitude: Optional[float] = field(
        default=None, converter=type_or_none(float), validator=coercible_to_float
    )
    longitude: Optional[float] = field(
        default=None, converter=type_or_none(float), validator=coercible_to_float
    )

    def api_dict(self):
        data = {"lat": self.latitude, "lon": self.longitude}
        return data

    def repr_for_report(self):
        return f"[{self.latitude},{self.longitude}]"

    def empty(self):
        return False


@attr.s(auto_attribs=True)
class TGSleepMessage(AbstractMessage):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    seconds: int = field(
        default=1, converter=convert_to_less_than_20s, validator=[gte(1), lte(20)]
    )
    show_typing: bool = field(default=False)

    def api_dict(self):
        return {"wait_seconds": self.seconds}

    def empty(self):
        return False

    def repr_for_report(self):
        return ""


@attr.s(auto_attribs=True)
class TGQRButtonGroup(AbstractQRButtonGroup):
    """
    Группа кнопок Telegram. Должны быть списком списков
    """

    buttons: List = field(
        converter=_convert_to_list_of_lists,
        renderable=True,
        has_buttons=True,
        localizable=True,
    )
    restore_normal_keyboard: bool = field(default=False)
    one_time_keyboard: bool = field(default=True)

    def __bool__(self):
        """
        Объект непустой, если есть хотя бы одна кнопка
        или если есть инструкция сбросить клавиатуру
        """
        btns = self.buttons
        return bool(
            (isinstance(btns, list) and len(btns) and len(btns[0]))
            or self.restore_normal_keyboard
        )

    @buttons.default
    def buttons_default(self):
        return [[]]

    @buttons.validator
    def must_be_list_of_lists_of_tgqrbutton(self, _, value):
        """
        Если передано что-то, кроме списка списков TGQRButton,
        - ломаемся
        """
        if value:
            try:
                for row in value:
                    for button in row:
                        assert isinstance(button, TGQRButton)
            except (AssertionError, TypeError, IndexError):
                self.raise_(
                    f"Buttons must be list of lists: [[TGQRButton]]," f"not {value}"
                )

    @buttons.validator
    def one_of_two_attrs(self, *args, **kwargs):
        """
        Должен быть передан либо указатель "вернуть оригинальную клавиатуру",
        либо новая клавиатура
        """
        btns = self.buttons
        buttons_exist = isinstance(btns, list) and len(btns) and len(btns[0])
        if self.restore_normal_keyboard and buttons_exist:
            self.raise_(
                "Please specify either restore_normal_keyboard or buttons, "
                "not both at the same time."
            )

    # Да, этот может вернуть лист, а не только дикт
    def api_dict(self) -> Union[list, dict]:  # type: ignore
        if self.buttons:
            return [[button.api_dict() for button in row] for row in self.buttons]
        else:
            return {"buttons": "hide QR keyboard"}


@attr.s
class TGQRButton(AbstractButton):
    """
    Группа кнопок Telegram. Должны быть списком списков
    """

    allowed_types = [
        AbstractButton.ACTION,
        AbstractButton.SHARE_LOCATION,
        AbstractButton.SHARE_CONTACT,
    ]

    def validate_data(self, attribute, value):
        """self.data может быть пустым. Это некритично для TGQR """

    def api_dict(self):
        data = {"text": self.text}
        if self.type == AbstractButton.SHARE_CONTACT:
            data.update({"request_contact": True})
        if self.type == AbstractButton.SHARE_LOCATION:
            data.update({"request_location": True})
        return data


# создаем дочерний класс, кот. наследует абстрактному классу,
# чтобы тестить на нем (ведь абстракный класс не инициализировать напрямую)
class ChildButton(AbstractButton):
    allowed_types = [AbstractButton.ACTION]

    def api_dict(self):
        pass


class AnswerCallbackQuery(object):
    """
    Ответ на CallbackQuery.

    query_id: ID callback query
    text: чем отвечать
    blocking: показывать ли блокирующий алерт (который требует клика)
    """

    def __init__(self, query_id: str, text: str, blocking: bool = False) -> None:
        self.query_id = query_id
        self.text = str(text)
        self.blocking = blocking

    def __repr__(self):
        return self.text
