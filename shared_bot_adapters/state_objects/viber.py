from typing import List, Optional, cast, Any

import attr

from .common import (
    AbstractButton,
    AbstractCard,
    AbstractMessage,
    AbstractQRButtonGroup,
    AbstractStateObject,
    clean_slashR_and_tabs,
    field,
)
from .converters import convert_to_less_than_20s, type_or_none
from .validators import gte, is_instance_of, list_of, lte, one_of
from ..utils import encode_query_params


class VBColors:
    BLACK = "#000000"
    DARK_PURPLE = "#59267c"
    LIGHT_PURPLE = "#8f5db7"
    PINK = "#e2d4e7"
    LIGHT_BLUE = "#a5cfd5"
    GREEN = "#81cd50"
    DARK_GREEN = "#019e55"
    CUSTOM_GREY = "#777777"
    WHITE = "#ffffff"
    CUSTOM_LIGHT_PINK = "#fcf2ff"
    CUSTOM_GREY_MENU_BG = "#e9e9e9"


def colored(string: str, color: Optional[str]) -> str:
    """Обернуть текст в <font> определенного цвета"""
    if color:
        return '<font color="{}">{}</font>'.format(color, string)
    return string


def bold(string: str) -> str:
    return "<b>{}</b>".format(string)


class VBMessage(AbstractMessage):
    pass


@attr.s(auto_attribs=True)
class VBTextMessage(VBMessage):
    """
    Текстовое сообщение. Содержит всего один атрибут: текст content.
    """

    text: str = field(default="", converter=str, renderable=True, localizable=True)

    def api_dict(self):
        data = {"text": self.text}
        return data

    def empty(self):
        return not self.text

    def repr_for_report(self):
        return self.text

    def post_render_clean(self):
        """
        Почистить контент после рендера
        """
        self.text = clean_slashR_and_tabs(self.text)
        # Укорачиваем капшен до разрешенных в вайбере 120 символов
        self.text = self.text[:1024]


@attr.s(auto_attribs=True)
class VBImageMessage(VBMessage):
    """
    Сообщение с изображением. Содержит атрибут: url, caption
    """

    url: Optional[str] = field(default=None, renderable=True)
    caption: Optional[str] = field(
        default="", converter=type_or_none(str), renderable=True, localizable=True
    )

    def api_dict(self):
        data = {"url": self.url, "caption": self.caption}
        return data

    def empty(self):
        return not self.url

    def repr_for_report(self):
        return self.url

    def post_render_clean(self):
        """Укорачиваем капшен до разрешенных в вайбере 120 символов"""
        if self.caption:
            self.caption = self.caption[:120]


@attr.s(auto_attribs=True)
class VBVideoMessage(VBImageMessage):
    """
    Сообщение с видео.
    """

    # размер видео - не больше 50
    size: Optional[int] = field(
        default=None,
        converter=type_or_none(int),
        validator=lte(50 * 1024 * 1024, nullable=True),
    )
    # длина видео - не больше 180 секунд
    duration: Optional[int] = field(
        default=None, converter=type_or_none(int), validator=lte(180, nullable=True)
    )

    def api_dict(self):
        # Required fields
        data = {"url": self.url, "size": self.size}
        # Optional fields
        if self.duration:
            data["duration"] = self.duration
        if self.caption:
            data["caption"] = self.caption
        return data

    def repr_for_report(self):
        return self.url


@attr.s(auto_attribs=True)
class VBDocumentMessage(VBImageMessage):
    """
    Сообщение с файлом. Содержит атрибут: url
    """

    size: Optional[int] = field(default=None)

    def change_class(self, cls: Any) -> None:
        """ Сменить класс (с документа на изображение/аудио/и тд"""
        self.__class__ = cls

    def api_dict(self):
        return {"url": self.url}

    def repr_for_report(self):
        return self.url


@attr.s
class VBAudioMessage(VBDocumentMessage):
    """
    Сообщение с аудио. Содержит атрибут: url
    У Viber нет типа объекта аудио, поэтому отсылаем просто файл
    """


@attr.s(auto_attribs=True)
class VBQRButton(AbstractButton):
    """
    QR-Кнопка Viber.
    """

    allowed_types = [
        AbstractButton.ACTION,
        AbstractButton.URL,
        AbstractButton.SHARE_CONTACT,
        AbstractButton.SHARE_LOCATION,
        AbstractButton.NONE,
    ]

    TEXT_SIZE_SMALL = "small"
    TEXT_SIZE_REGULAR = "regular"
    TEXT_SIZE_LARGE = "large"

    VALIGN_TOP = "top"
    VALIGN_MIDDLE = "middle"
    VALIGN_BOTTOM = "bottom"

    HALIGN_LEFT = "left"
    HALIGN_CENTER = "center"
    HALIGN_RIGHT = "right"

    INTERNAL = "internal"
    EXTERNAL = "external"

    BROWSER_MODE_FULLSCREEN = "fullscreen"
    BROWSER_MODE_FULLSCREEN_PORTRAIT = "fullscreen-portrait"
    BROWSER_MODE_FULLSCREEN_LANDSCAPE = "fullscreen-landscape"
    BROWSER_MODE_PARTIAL_SIZE = "partial-size"
    BROWSER_MODE_CHOICES = (
        BROWSER_MODE_FULLSCREEN,
        BROWSER_MODE_FULLSCREEN_PORTRAIT,
        BROWSER_MODE_FULLSCREEN_LANDSCAPE,
        BROWSER_MODE_PARTIAL_SIZE,
    )

    names_normalization_dict = {
        "image_url": ["image"],
        "height": ["rows"],
        "width": ["columns"],
    }

    height: int = field(default=1)
    width: int = field(default=6)
    opacity: Optional[int] = field(default=None)
    horizontal_align: Optional[str] = field(
        default=None, validator=one_of([None, HALIGN_LEFT, HALIGN_CENTER, HALIGN_RIGHT])
    )
    vertical_align: Optional[str] = field(
        default=None, validator=one_of([None, VALIGN_TOP, VALIGN_MIDDLE, VALIGN_BOTTOM])
    )
    image_url: Optional[str] = field(default=None, renderable=True)
    silent: bool = field(default=False, converter=bool)
    text_size: Optional[str] = field(
        default=None,
        validator=one_of([None, TEXT_SIZE_SMALL, TEXT_SIZE_REGULAR, TEXT_SIZE_LARGE]),
    )
    bg_color: str = field(default=None)
    font_color: str = field(default=None)
    open_url_type: str = field(default=INTERNAL, validator=one_of([INTERNAL, EXTERNAL]))
    webview_height_ratio: Optional[str] = field(
        default=None, validator=one_of({None, "compact"})
    )
    browser_mode: str = field(
        none_to_default=True,
        default=BROWSER_MODE_FULLSCREEN,
        validator=one_of(BROWSER_MODE_CHOICES + (None,)),
    )
    browser_title: Optional[str] = field(
        converter=lambda v: str(v)[:15] if v else None, default=None
    )

    def validate_text(self, attribute, value):
        """Текст в кнопке вайбера может быть пустым"""

    def validate_data(self, attribute, value):
        if (
            self.type
            not in [
                AbstractButton.NONE,
                AbstractButton.SHARE_CONTACT,
                AbstractButton.SHARE_LOCATION,
            ]
            and not value
        ):
            self.raise_('if type is not "none", you need to supply data.')

    def api_dict(self) -> dict:

        type_dict = {
            self.URL: "open-url",
            self.ACTION: "reply",
            self.SHARE_LOCATION: "location-picker",
            self.SHARE_CONTACT: "share-phone",
            self.NONE: "none",
        }

        if self.type == self.URL:
            # урлам энкодируем гет-параметры:
            # некоторые версии вайбера не пропускают не-англ символы в урлах
            self.data = encode_query_params(cast(str, self.data))

        data = {
            "Columns": self.width,
            "Rows": self.height,
            "ActionType": type_dict[self.type],  # type: ignore
        }
        if self.type == self.SHARE_LOCATION:
            data["ActionBody"] = "location"
        elif self.type == self.SHARE_CONTACT:
            data["ActionBody"] = "contact"
        else:
            # для иных типов, кроме отправки телефона и локации,
            # - передаем ActionBody
            data["ActionBody"] = self.data

        if self.type == self.URL:
            # для кнопок-ссылок передаем тип вебвьюхи: встроенная вайберская
            # или выход на нативный браузер.
            # Это новый атрибут, поэтому забираем через getattr -
            # в старых JSON-ах его может не быть.
            data["OpenURLType"] = self.open_url_type

            if self.webview_height_ratio == "compact":
                # аналогично ФБ даем возможность откыть вебвью на полэкрана
                data["InternalBrowser"] = {"Mode": "partial-size"}

        if self.text is not None:
            data["Text"] = colored(self.text, self.font_color)
        if self.image_url:
            data["Image"] = self.image_url
        if self.bg_color is not None:
            data["BgColor"] = self.bg_color
        if self.opacity is not None:
            data["TextOpacity"] = self.opacity
        if self.horizontal_align:
            data["TextHAlign"] = self.horizontal_align
        if self.vertical_align:
            data["TextVAlign"] = self.vertical_align
        if self.silent:
            data["Silent"] = self.silent
        if self.text_size:
            data["TextSize"] = self.text_size

        if self.type == self.URL:
            # Поведение встроенного браузера
            data["InternalBrowser"] = {"Mode": self.browser_mode}
            if self.browser_title:
                data["InternalBrowser"][  # type: ignore
                    "CustomTitle"
                ] = self.browser_title
        return data


@attr.s
class VBCardButton(VBQRButton):
    """
    Кнопка для карточки Viber.
    Отличается от  QR только разрешеным типом действий
    """

    allowed_types = [AbstractButton.ACTION, AbstractButton.URL, AbstractButton.NONE]


@attr.s(auto_attribs=True)
class VBQRButtonGroup(AbstractQRButtonGroup):
    """
    Группа кнопок Viber. Должны быть списком
    """

    KEYBOARD_MODE_REGULAR = "regular"
    KEYBOARD_MODE_MINIMIZED = "minimized"
    KEYBOARD_MODE_HIDDEN = "hidden"
    KEYBOARD_MODE_CHOICES = (
        KEYBOARD_MODE_REGULAR,
        KEYBOARD_MODE_MINIMIZED,
        KEYBOARD_MODE_HIDDEN,
    )

    buttons: List[VBQRButton] = field(
        factory=list,
        validator=list_of(VBQRButton, maxlen=100),
        renderable=True,
        has_buttons=True,
        localizable=True,
    )
    restore_normal_keyboard: bool = field(default=False)
    bg_color: Optional[str] = field(default=None)
    keyboard_mode: str = field(
        default=KEYBOARD_MODE_REGULAR, validator=one_of(KEYBOARD_MODE_CHOICES)
    )

    @buttons.validator
    def one_of_two_attrs(self, *args, **kwargs):
        """
        Должен быть передан либо указатель "вернуть оригинальную клавиатуру",
        либо новая клавиатура
        """
        if self.restore_normal_keyboard and self.buttons:
            self.raise_(
                "Please specify either restore_normal_keyboard or buttons, "
                "not both at the same time."
            )

    def api_dict(self) -> Optional[dict]:
        if self.buttons:
            btn_data = [button.api_dict() for button in self.buttons]
            return {
                "Type": "keyboard",
                "DefaultHeight": False,
                "Buttons": btn_data,
                "BgColor": self.bg_color,
                "InputFieldState": self.keyboard_mode,
            }
        return None


@attr.s(auto_attribs=True)
class VBSleepMessage(VBMessage):
    """
    Сообщение-пауза. Содержит значение паузы в секундах. Максимум - 20 секунд.
    """

    seconds: int = field(
        default=1, converter=convert_to_less_than_20s, validator=[gte(1), lte(20)]
    )
    show_typing: bool = field(default=False)

    def api_dict(self):
        return {"wait_seconds": self.seconds}

    def empty(self):
        return False

    def repr_for_report(self):
        return ""


@attr.s(auto_attribs=True)
class ButtonRowEmulator:
    """
    Вспомогательый класс, который позволяет посчитать высоту списка кнопок
    независимо от того, в один ряд они выстроены или в несколько.
    Единственное ограничение: высота  кнопок в одном ряду должна быть одинаковой, иначе
    это будут уже очень сложные расчеты.
    """

    rows: list = attr.ib(factory=list)

    def get_total_height(self) -> int:
        return sum(row[0].height for row in self.rows)

    def put_buttons(self, buttons: List[VBCardButton]) -> None:

        for button in buttons:
            self.add_button(button)

    def add_button(self, button: VBCardButton) -> None:
        if len(self.rows) == 0:
            self.rows.append([button])
            return

        total_row_width = sum(button.width for button in self.rows[-1])
        if total_row_width + button.width <= 6:
            self.rows[-1].append(button)
            heights = [button.height for button in self.rows[-1]]
            if not all(height == self.rows[-1][0].height for height in heights):
                raise Exception(
                    f"All buttons in a row must be of same height! Now heights are {heights}"
                )
        else:
            self.rows.append([button])


@attr.s(auto_attribs=True)
class VBCard(AbstractCard):
    """
    Сообщение-карточка вайбера
    """

    # перед дампом в строку нужно стереть атрибут _group,
    # являющийся обратной ссылкой на группу карточек
    delete_before_dump = ["_group"]

    names_normalization_dict = {
        "image_url": ["image"],
        "subtitle": ["description"],
        "subtitle_color": ["description_color"],
    }

    title: str = field(
        default=None, converter=type_or_none(str), renderable=True, localizable=True
    )
    subtitle: str = field(
        default=None, converter=type_or_none(str), renderable=True, localizable=True
    )
    image_url: str = field(default="", renderable=True)
    buttons: List[VBCardButton] = field(
        factory=list,
        validator=list_of(VBCardButton, maxlen=7),
        renderable=True,
        has_buttons=True,
        none_to_default=True,
        localizable=True,
    )
    image_height_rows: int = field(default=3, none_to_default=True)
    item_url: str = field(default=None, renderable=True)
    title_color: str = field(default=VBColors.DARK_PURPLE)
    subtitle_color: str = field(default=VBColors.CUSTOM_GREY)

    # то, чего нет в конструкторе: определение действия по клику на картинку
    # (может вести себя как кнопка)
    # image_action_type: str = field(default=AbstractButton.NONE, renderable=True)
    # image_action_data: Optional[str] = field(default=None, renderable=True)
    # image_action_silent: bool = field(default=True, converter=bool)
    # image_action_text: str = field(
    #     default="", converter=type_or_none(str), renderable=True, localizable=True
    # )

    # @image_action_type.validator
    # def type_validator(self, attribute, value):
    #     """
    #     Валидация типа кнопки.
    #     Типы кнопки определяются в дочерних классах.
    #     """
    #     allowed_types = [
    #         AbstractButton.ACTION,
    #         AbstractButton.URL,
    #         AbstractButton.NONE,
    #     ]
    #     if value not in allowed_types:
    #         self.raise_(
    #             f"Wrong image_action_type: {value}. " f"Must be one of {allowed_types}"
    #         )

    @image_height_rows.validator
    def must_be_int(self, attribute, value):
        if not isinstance(value, int):
            self.raise_("image_height_rows must be int")

    @image_height_rows.validator
    def must_be_positive_if_image_exists(self, attribute, value):
        if value == 0 and self.image_url:
            self.raise_(
                "If image_url is set, then image_height_rows " "cannot be equal to 0"
            )

    def rebuild_validate(self):
        """
        Сначала обрабатываем легаси:
        всем кнопкам класса QR насильно меняем класс на CardButton
        """

        if hasattr(self, "buttons") and isinstance(self.buttons, list):
            for button in self.buttons:
                if button.__class__ == VBQRButton:
                    button.__class__ = VBCardButton
        super().rebuild_validate()

    def api_dict(self):
        raise NotImplementedError

    def __attrs_post_init__(self):
        # проверяем настройку высоты картинки
        if not self.image_url:
            self.image_height_rows = 0

    @property
    def group(self):
        """
        Делаем ссылку на группу через @property, чтобы иметь возможность
        сообщить пользователю, если он пытается невовремя получить доступ
        к группе.
        """
        if not self._group:
            raise AttributeError(
                "This VBCard is not bound to a group and thus does not have "
                "access to .group attribute."
            )
        return self._group

    @group.setter
    def group(self, value):
        self._group = value

    def to_buttons(self) -> List[VBCardButton]:
        """
        Превратить объект карточки в вайберовский список Buttons
        """
        all_buttons = self.buttons

        image_height_rows = self.image_height_rows or 0

        emulator = ButtonRowEmulator()
        emulator.put_buttons(all_buttons)
        button_height = emulator.get_total_height()

        # считаем высоту кнопки, отведенной под текст,
        # добавляем ее к уже имеющимся кнопкам
        text_height_rows = self.group.card_height - button_height - image_height_rows
        if text_height_rows:
            # текст кнопки с "текстом" может как содержать заголовок
            # и описание, так и быть пустым
            text = ""
            if self.title:
                text += bold(colored(self.title, self.title_color))
            if self.subtitle:
                text += "<br>" + colored(self.subtitle, self.subtitle_color)
            text_btn = VBCardButton(
                text=text,
                vertical_align="top",
                horizontal_align="left",
                width=self.group.card_width,
                height=text_height_rows,
                type=VBQRButton.NONE,
                text_size="small",
                silent=True,
            )
            if self.item_url:
                text_btn.type = VBQRButton.URL
                text_btn.data = self.item_url

            all_buttons = [text_btn] + all_buttons

        # если в карточке есть изображение
        # (или указана высота изображения, при этом самого изображения нет) -
        # формируем кнопку и вставляем перед накопленными ранее кнопками
        if self.image_url or image_height_rows:
            image_btn = VBCardButton(
                text="",
                image_url=self.image_url,
                height=image_height_rows,
                width=self.group.card_width,
                type=VBQRButton.NONE,
                silent=True,
            )
            if self.item_url:
                image_btn.type = VBQRButton.URL
                image_btn.data = self.item_url

            all_buttons = [image_btn] + all_buttons

        # if self.image_url or image_height_rows:
        #     image_btn = VBCardButton(
        #         text=self.image_action_text,
        #         opacity=0,
        #         image_url=self.image_url,
        #         height=image_height_rows,
        #         width=self.group.card_width,
        #         type=self.image_action_type,
        #         data=self.image_action_data,
        #         silent=self.image_action_silent,
        #     )
        #
        #     all_buttons = [image_btn] + all_buttons

        return all_buttons

    def repr_for_report(self):
        repr = self.title or ""
        if self.subtitle:
            repr += "\n" + self.subtitle
        return repr


@attr.s(auto_attribs=True)
class VBCardGroup(AbstractStateObject):
    """
    Группа карточек вайбера. Превращает список карточек в список Buttons
    согласно АПИ вайбера
    """

    cards: List[VBCard] = field(
        factory=list,
        validator=list_of(VBCard, maxlen=6),
        renderable=True,
        has_buttons=True,
        has_cards=True,
        localizable=True,
    )
    card_width: int = field(
        default=6, converter=type_or_none(int), validator=[gte(1), lte(6)]
    )
    card_height: int = field(
        default=7, converter=type_or_none(int), validator=[gte(1), lte(7)]
    )
    bg_color: Optional[str] = field(default=None)

    def __attrs_post_init__(self):
        """
        Добавляем к каждой карточке ссылку на группу для корректного рендера
        """
        for card in self.cards:
            card.group = self

    def api_dict(self) -> list:
        """
        Собрать результаты вызова to_buttons() во всех карточках
        в единый список, перевести в dict
        """
        buttons: List[VBCardButton] = []
        for card in self.cards:
            buttons += card.to_buttons()
        api_dict = [btn.api_dict() for btn in buttons]
        return api_dict

    def rebuild_validate(self):
        """
        После ребилда присваиваем всем детям ссылку на себя и проверяем высоту
        """
        super().rebuild_validate()
        for card in self.cards:
            card.group = self

            # Убеждаемся, что в каждой карточке
            # кнопки соответствуют ширине карточки. Ставим корректную ширину
            for btn in card.buttons:
                btn.width = self.card_width

            # если у какой-то из карточек кол-во кнопок больше, чем общая
            # заданная высота, - ошибка
            if self.card_height < len(card.buttons):
                self.raise_(
                    f"Card height ({self.card_height}) "
                    f"is less then number of card buttons "
                    f"({len(card.buttons)}). Bad card: {card}"
                )


@attr.s(auto_attribs=True)
class VBCarouselMessage(VBMessage):
    """Карусель вайбера. Содержит до 6 карточек."""

    card_group: Optional[VBCardGroup] = field(
        default=None,
        validator=is_instance_of(VBCardGroup),
        renderable=True,
        has_buttons=True,
        has_cards=True,
        localizable=True,
    )

    def api_dict(self) -> dict:
        if self.card_group:
            return {
                "ButtonsGroupColumns": self.card_group.card_width,
                "ButtonsGroupRows": self.card_group.card_height,
                "Buttons": self.card_group.api_dict(),
                "BgColor": self.card_group.bg_color,
            }
        return {}

    def empty(self):
        return not self.card_group.cards

    def repr_for_report(self):
        card_reprs = [card.repr_for_report() for card in self.card_group.cards]
        card_reprs = [r for r in card_reprs if r]
        return "\n".join(card_reprs)


@attr.s(auto_attribs=True)
class VBLocationMessage(VBMessage):
    """
    Геолокация
    :param latitude: широта, float
    :param longitude: долгота, float
    """

    def coercible_to_float(self, attribute, value):
        try:
            float(value)
        except (ValueError, TypeError):
            self.raise_(f"{attribute.name} must be float")

    latitude: Optional[float] = field(
        default=None, converter=type_or_none(float), validator=coercible_to_float
    )
    longitude: Optional[float] = field(
        default=None, converter=type_or_none(float), validator=coercible_to_float
    )

    def empty(self):
        return False

    def api_dict(self):
        return {
            "lat": self.latitude,
            "lon": self.longitude,
            "extra": "This dict is not used in Viber API. "
            "It is here for universal purposes",
        }

    def repr_for_report(self):
        return f"[{self.latitude},{self.longitude}]"
