from dataclasses import dataclass
from typing import List, Optional

from shared_bot_adapters.parsers.parse import TELEGRAM, VIBER, VKONTAKTE
from shared_bot_adapters.state_objects import *
from shared_bot_adapters.state_objects.common import AbstractQRButtonGroup


def build_text(text: str, platform: str) -> BotState:
    classes = {
        TELEGRAM: TGTextMessage,
        VIBER: VBTextMessage,
        VKONTAKTE: VKTextMessage,
    }
    Message = classes[platform.lower()]
    return BotState(messages=[Message(text=text)])


@dataclass
class Button:
    text: str
    data: str
    viber_text_color: str = "#000000"
    viber_bg_color: str = "#ffffff"


@dataclass
class ButtonContainer:
    buttons: List[List[Button]]


def build_buttons(
    button_container: ButtonContainer,
    platform: str,
    telegram_one_time_keyboard: bool = False,
    viber_bg_color: Optional[str] = "#0076e0",
    viber_text_color: Optional[str] = "#ffffff",
) -> AbstractQRButtonGroup:
    """
    Превращаем универсальный контейнер с кнопками в кнопки конкретной платформы.
    Для ТГ и ВК ничего преобразовывать не нужно; для вайбера подсчитываем ширину
    кнопок (поддерживается 1, 2 и 3 кнопки в ряду).
    """
    if platform == TELEGRAM:
        tg_buttons = [
            [
                TGQRButton(text=btn.text, data=btn.data, type=TGQRButton.ACTION)
                for btn in row
            ]
            for row in button_container.buttons
        ]
        return TGQRButtonGroup(
            one_time_keyboard=telegram_one_time_keyboard, buttons=tg_buttons
        )

    if platform == VKONTAKTE:
        vk_buttons = [
            [
                VKButton(text=btn.text, data=btn.data, type=VKButton.ACTION)
                for btn in row
            ]
            for row in button_container.buttons
        ]
        return VKQRButtonGroup(buttons=vk_buttons)

    if platform == VIBER:
        list_ = []
        for row in button_container.buttons:
            if len(row) == 1:
                width = 6
            elif len(row) == 2:
                width = 3
            elif len(row) == 3:
                width = 2
            else:
                raise Exception(f"Unsupported amount of buttons in row: {len(row)}")
            buttons = [
                VBQRButton(
                    text=btn.text,
                    data=btn.data,
                    type=VBQRButton.ACTION,
                    width=width,
                    font_color=viber_text_color or btn.viber_text_color,
                    bg_color=viber_bg_color or btn.viber_bg_color,
                )
                for btn in row
            ]
            list_ += buttons
        return VBQRButtonGroup(buttons=list_)

    raise Exception(f"Unsupported platform: {platform}")
