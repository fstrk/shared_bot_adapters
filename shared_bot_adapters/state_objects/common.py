import json
import logging
import re
from abc import ABC, abstractmethod
from json import JSONDecodeError
from typing import Any, Callable, Generator, Iterator, Optional, Type, TypeVar, Union


import attr
from attr import Factory

from ..exceptions import StateObjectError
from .validators import is_instance_of

logger = logging.getLogger(__name__)

T = TypeVar("T")


def field(
    renderable: bool = False,
    has_buttons: bool = False,
    has_cards: bool = False,
    localizable: bool = False,
    none_to_default: bool = False,
    **kwargs: Any,
) -> Any:
    """
    Кастомный атрибут BotState-объектов. Отличается от обычного атрибута
    несколькими аргументами, которые попадают в метаданные атрибута.

    :param renderable: рендерить ли это поле при вызове render()
    :param localizable: переводить ли это поле при вызове localize()
    :param has_buttons: искать ли в этом поле кнопки при вызове get_buttons()
    :param has_cards: искать ли в этом поле кнопки при вызове get_cards()
    :param none_to_default: применять ли дефолт к этому атрибуту,
        если его значение None.
        Позволяет обработать ситуации, когда, например,
        в json-представлении какой-то атрибут равен null,
        и при ребилде он не подменяется на дефолт, т.к. вроде бы присутствует,
        хоть и нулевой. Иногда null - некорректное значение, которое не проходит
        валидацию.

    """
    kwargs["metadata"] = {
        "renderable": renderable,
        "has_buttons": has_buttons,
        "has_cards": has_cards,
        "none_to_default": none_to_default,
        "localizable": localizable,
    }
    return attr.ib(**kwargs)


def clean_slashR_and_tabs(string):
    """
    Заменить все включения \r\n на \n,
    стереть стартовые табуляции и пробелы в каждой строке
    (это из-за того, что в шаблонах после рендера
    иногда остается много пробелов и табов)
    """
    string = re.sub(r"\r\n", r"\n", string)
    lines = string.splitlines()
    lines = [l.strip() for l in lines]
    string = "\n".join(lines)
    return string


def get_abstract_objects(value: Any) -> Iterator["AbstractStateObject"]:
    """
    Хелпер, позволяющий доставать из сложной структуры
    (например, сущность, или список сущностей, или спискок списков, ...)
    объекты AbstractStateOject по одному
    """
    if isinstance(value, list):
        for item in value:
            # для списков вызываем этот же хелпер рекурсивно
            yield from get_abstract_objects(item)

    elif isinstance(value, AbstractStateObject):
        # для конкретных объектов - yield-им их
        yield value


def clear_invalid_escapes(string: str) -> str:
    """
    Функция очистки JSON-строки от некорректных эскейпов.

    формат JSON поддерживает всего несколько экранированных символов
    (это не регексы, это свой формат, просто немного похожий на регексы):
        \t   \n    \r    \\    \"

    поэтому, если нам встретится строчка {"text": "цена 10 руб\мес"},
    это невозможно будет распарсить, т.к. парсер json.loads посчитает,
    что \м - это экранирование, и сломается.

    Поэтому мы находим все невалидные экранирования и экранируем их:
    дописываем бэкслеш перед слешом,чтобы \м  превратить в \\м
    """
    escaped = re.findall(r"\\.", string)
    for item in escaped:
        if item[1] not in ["t", "n", "r", "\\", '"']:
            string = re.sub(r"\\" + item[1], r"\\\\" + item[1], string)

    return string


class AbstractStateObject(ABC):
    def _fields_with_metadata(self, **kwargs: Any) -> Iterator[attr.Attribute]:
        """Вернуть все атрибуты, соответсвующие определенным метаданным"""
        for a in attr.fields(self.__class__):
            metadata = a.metadata
            if all(metadata.get(key) == value for key, value in kwargs.items()):
                yield a

    def clean_before_dump(self) -> None:
        """
        Почистить объект перед дампом в строку.
        Для этого берем все поля, которые перечислены в self.delete_before_dump,
        стираем их, а потом идем по оставшимся атрибутам рекурсивно.
        """
        delete_before_dump = getattr(self, "delete_before_dump", [])
        for field_name in delete_before_dump:
            delattr(self, field_name)

        for a in attr.fields(self.__class__):
            value = getattr(self, a.name, None)
            for obj in get_abstract_objects(value):
                obj.clean_before_dump()

    def get_all_state_objects(self) -> Iterator["AbstractStateObject"]:
        """ Генератор вообще всех state_object-ов, из которых состоит ботстейт """
        for field in self._fields_with_metadata():
            value = getattr(self, field.name)
            # из каждого поля достаем плоский список AbstractStateObject-ов
            for obj in get_abstract_objects(value):
                if isinstance(obj, AbstractStateObject):
                    # возващаем и сам объект, и его детей
                    yield obj
                    yield from obj.get_all_state_objects()

    def get_buttons(self) -> Iterator["AbstractButton"]:
        """
        Генератор, который ходит по иерархии AbstractStateObject-ов
        и yield-ит кнопки.
        Чтобы генератор зашел в определенное поле и поискал там кнопки,
        нужно, чтобы это поле было помечено как field(has_buttons=True).
        """
        for field in self._fields_with_metadata(has_buttons=True):
            value = getattr(self, field.name)
            # из каждого поля достаем плоский список AbstractStateObject-ов
            for obj in get_abstract_objects(value):
                if isinstance(obj, AbstractButton):
                    # если этот объект - кнопка - yield ее
                    yield obj
                else:
                    # иначе рекурсивно спускаемся в объект
                    # и уже внутри него ищем кнопки
                    yield from obj.get_buttons()

    def get_cards(self):
        """
        Генератор, который ходит по иерархии AbstractStateObject-ов
        и yield-ит карточки.
        Чтобы генератор зашел в определенное поле и поискал там карточки,
        нужно, чтобы это поле было помечено как field(has_cards=True).
        """
        for field in self._fields_with_metadata(has_cards=True):
            value = getattr(self, field.name)
            # из каждого поля достаем плоский список AbstractStateObject-ов
            for obj in get_abstract_objects(value):
                if isinstance(obj, AbstractCard):
                    # если этот объект - карточка - yield ее
                    yield obj
                else:
                    # иначе рекурсивно спускаемся в объект
                    # и уже внутри него ищем карточки
                    yield from obj.get_cards()

    def raise_(self, msg: str, msg_for_frontend: Optional[str] = None) -> None:
        msg = f"Error in {self.__class__.__name__}: {msg}"
        raise StateObjectError(msg, msg_for_frontend)


class AbstractMessage(AbstractStateObject, ABC):
    """
    Класс, от которго наследуются все типы сообщений.

    Абстрактный класс с абстрактными методами, гарантирующими,
    что они будут переопределены во всех наследниках.
    """

    @abstractmethod
    def empty(self) -> bool:
        pass

    @abstractmethod
    def repr_for_report(self) -> str:
        pass

    def class_name(self):
        return self.__class__.__name__


class AbstractQRButtonGroup(AbstractStateObject):
    pass


class AbstractCard(AbstractStateObject):
    pass


class AbstractTriggerWordGroup(AbstractStateObject):
    pass


@attr.s(auto_attribs=True)
class ButtonMetaData(AbstractStateObject):
    """
    Метаданные для кнопки: какие куки установить,
    в зависимости от каких - рендерить и пр.
    """

    save_cookies: dict = field(factory=dict, validator=is_instance_of(dict))
    jwt_payload: dict = field(factory=dict, validator=is_instance_of(dict))

    def api_dict(self):
        return {"save_cookies": self.save_cookies, "jwt_payload": self.jwt_payload}

    def __bool__(self):
        """
        Метаданные не False, только если составляющие их словари не False
        """
        return bool(self.save_cookies) or bool(self.jwt_payload)


# ниже идет переопределение type, поэтому сохраняем тип здесь
NoneType = type(None)


@attr.s(auto_attribs=True)
class ButtonSystemMetaData:
    """
    Системные метаданные, недоступные для имплементации.
    Например, каким узлом была сгенерена кнопка
    """

    generated_by_node_pk: Optional[int] = field(
        default=None, validator=is_instance_of((NoneType, int))  # type: ignore
    )
    button_text: Optional[str] = field(default=None)


@attr.s(auto_attribs=True)
class AbstractButton(AbstractStateObject, ABC):
    delete_before_dump = ["system_meta"]

    text: str = field(default="", converter=str, renderable=True, localizable=True)
    data: Optional[str] = field(default=None, renderable=True)
    type: Optional[str] = field(default=None, renderable=True)
    meta: Optional[ButtonMetaData] = field(
        default=None,
        validator=is_instance_of((NoneType, ButtonMetaData)),  # type: ignore
    )
    system_meta: Optional[ButtonSystemMetaData] = field(
        default=None,
        validator=is_instance_of((NoneType, ButtonSystemMetaData)),  # type: ignore
    )

    ACTION = "action"
    URL = "url"
    SHARE_CONTACT = "share_contact"
    SHARE_LOCATION = "share_location"
    SHARE_BOT = "share_bot"
    CALL_PHONE = "call_phone"
    NONE = "none"

    names_normalization_dict = {"text": ["name", "title"], "data": ["payload"]}

    def _must_exist(self, attr, val):
        if not val:
            raise StateObjectError(f"{self.__class__.__name__}.{attr.name} must exist")

    # Создаем два метода, которые можно будет переопределить в наследниках
    validate_text = validate_data = _must_exist

    @text.validator
    def text_must_be_set(self, attr, val):
        return self.validate_text(attr, val)

    @data.validator
    def data_must_be_set(self, attr, val):
        return self.validate_data(attr, val)

    @type.validator
    def type_validator(self, attribute, value):
        """
        Валидация типа кнопки.
        Типы кнопки определяются в дочерних классах.
        """
        if value not in self.allowed_types:
            raise StateObjectError(
                f"Wrong {self.__class__.__name__} type: {value}. "
                f"Must be one of {self.allowed_types}"
            )

    def set_system_meta(self, **kwargs: Any) -> None:
        if self.system_meta:
            self.system_meta = attr.evolve(self.system_meta, **kwargs)
        else:
            self.system_meta = ButtonSystemMetaData(**kwargs)

    @property
    @abstractmethod
    def allowed_types(self):
        """
        Атрибут должн быть определен на всех кнопках. Он решает,
        какие типы разрешены для той или иной кнопки
        """

    def data_with_meta(self):
        """
        Вернуть словарь c self.data и self.meta и self.system_meta
        (для упаковки и отправки в мессенджер)
        """
        payload = {"data": self.data}
        if self.meta:
            payload.update(attr.asdict(self.meta))
        if self.system_meta:
            payload.update(attr.asdict(self.system_meta))
        return payload


@attr.s
class FromCacheQRButtonGroup(AbstractQRButtonGroup):
    """
    Объект, который, будучи добавлен в стейт вместо QR-кнопок,
    является директивой для препроцессора
    достань QR-кнопки из кэша, в котором ты сохранил их в прошлый раз".

    Является наследником AbstractQRButtonGroup,
    чтобы не сломать валидатор BotState.
    """

    def api_dict(self):
        pass
