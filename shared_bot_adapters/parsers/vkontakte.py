import json
from typing import Dict

import attr

from .base_parser import (
    AbstractTokens,
    BaseParser,
    UnsupportedMessageError,
    for_attribute,
)


@attr.s(auto_attribs=True)
class VkontakteTokens(AbstractTokens):

    deeplink_params: dict = attr.ib(factory=dict)
    message_id: str = attr.ib(default="")


def _get_biggest_image_url(sizes_list: list) -> str:
    """ Получаем самое большое изображение из доступных """
    sorted_list = sorted(sizes_list, key=lambda item: item["width"] * item["height"])

    return sorted_list[-1]["url"]


class VkontakteParser(BaseParser):
    @staticmethod
    def get_message_object(data: dict) -> dict:
        """
        В версии 5.103 объект сообщения лежит внутри объекта object.
        В версии 5.80 объект object - это и есть сообщение.
        """
        obj = data["object"]

        if "message" in obj:
            return obj["message"]
        return obj

    @classmethod
    def _common_params(cls, message: dict) -> dict:

        message_object = cls.get_message_object(data=message)

        deeplink_params: Dict[str, str] = {}
        if message_object.get("ref"):
            deeplink_params = BaseParser.parse_deeplink_params(message_object["ref"])

        # обработка легаси формата собщений ВК
        if "from_id" in message_object:
            messenger_id = str(message_object["from_id"])
        else:
            messenger_id = str(message_object["user_id"])

        message_id = message_object["id"]

        return {
            "messenger_id": messenger_id,
            "message_id": message_id,
            "author_first_name": messenger_id,
            "author_last_name": messenger_id,
            "deeplink_params": deeplink_params,
        }

    @for_attribute("object.attachments[0].type", "photo")
    @for_attribute("object.message.attachments[0].type", "photo")
    def get_picture(self, message: dict) -> VkontakteTokens:
        message_object = self.get_message_object(message)

        attachment = message_object["attachments"][0]
        url = _get_biggest_image_url(attachment["photo"]["sizes"])

        return VkontakteTokens(  # type: ignore
            **self._common_params(message), content=url, type=VkontakteTokens.PICTURE
        )

    @for_attribute("object.attachments[0].type", "doc")
    @for_attribute("object.message.attachments[0].type", "doc")
    def get_document(self, message: dict) -> VkontakteTokens:
        message_object = self.get_message_object(message)

        document = message_object["attachments"][0]["doc"]
        content = document["url"]

        return VkontakteTokens(  # type: ignore
            **self._common_params(message),
            content=content,
            type=VkontakteTokens.DOCUMENT,
            file_info={"name": document.get("title"), "size": document.get("size")},
        )

    @for_attribute("object.attachments[0].type", "audio")
    @for_attribute("object.message.attachments[0].type", "audio")
    def get_audio(self, message: dict) -> VkontakteTokens:
        message_object = self.get_message_object(message)

        audio = message_object["attachments"][0]["audio"]
        content = audio["url"]

        return VkontakteTokens(  # type: ignore
            **self._common_params(message),
            content=content,
            type=VkontakteTokens.DOCUMENT,
        )

    @for_attribute("object.attachments[0].type", "video")
    @for_attribute("object.message.attachments[0].type", "video")
    def get_video(self, message: dict) -> VkontakteTokens:
        """
        Мы должны явно вызвать ошибку, чтобы этот тип сообщений не поймался
        хендлером текстовых сообщений
        (который среагирует на наличие object.text,
        который всегда есть у контактовских сообщений,
        и решит, что это текстовое сообщение)
        """
        raise UnsupportedMessageError(f"Videos are not supported. {message}")

    @for_attribute("object.attachments[0].type", "sticker")
    @for_attribute("object.message.attachments[0].type", "sticker")
    def get_sticker(self, message: dict) -> VkontakteTokens:
        """
        Мы должны явно вызвать ошибку, чтобы этот тип сообщений не поймался
        хендлером текстовых сообщений
        (который среагирует на наличие object.text,
        который всегда есть у контактовских сообщений,
        и решит, что это текстовое сообщение)
        """
        raise UnsupportedMessageError(f"Stickers are not supported. {message}")

    @for_attribute("object.geo")
    @for_attribute("object.message.geo")
    def get_geo(self, message: dict) -> VkontakteTokens:
        message_object = self.get_message_object(message)

        coordinates = message_object["geo"]["coordinates"]

        if "latitude" in coordinates:
            latitude = coordinates["latitude"]
            longitude = coordinates["longitude"]
        else:
            latitude, longitude = coordinates.split(" ")

        content = f"{latitude},{longitude}"
        type_specific_data = {"latitude": latitude, "longitude": longitude}

        return VkontakteTokens(  # type: ignore
            **self._common_params(message),
            content=content,
            type=VkontakteTokens.LOCATION,
            type_specific_data=type_specific_data,
        )

    def _get_text_tokens(self, message: dict) -> VkontakteTokens:
        """
        Парсер, который забирает текст сообщения из text или body (легаси ВК)
        и возвращает токены для текстового ботреквеста
        """
        message_object = self.get_message_object(message)

        # обработка легаси формата ВК
        if "text" in message_object:
            content = message_object["text"]
        else:
            content = message_object["body"]

        return VkontakteTokens(  # type: ignore
            **self._common_params(message), content=content, type=VkontakteTokens.TEXT
        )

    @for_attribute("object.payload")
    @for_attribute("object.message.payload")
    def get_payload(self, message: dict) -> VkontakteTokens:
        """
        этот хендлер должен идти предпоследним, т.к. у контакта
        почти все типы сообщений
        содержат object.payload и object.text/object.body.
        Поэтому парсинг пейлоада и текста должен происходить в самом конце,
        только если остальные хендлеры не сработали.
        """
        message_object = self.get_message_object(message)

        if message_object["payload"] is not False:

            payload_data = json.loads(message_object["payload"])
            payload = payload_data["command"]

            return VkontakteTokens(  # type: ignore
                **self._common_params(message),
                content=payload,
                type=VkontakteTokens.POSTBACK,
            )

        return self._get_text_tokens(message)

    @for_attribute("object.text")
    @for_attribute("object.message.text")
    def get_text(self, message: dict) -> VkontakteTokens:
        """
        этот хендлер должен идти последним и отрабаывать в том
        случае, когда у сообщения нет пейлоада.
        Это текстовые сообщения из старых клиентов ВК.
        """
        return self._get_text_tokens(message)

    @for_attribute("object.body")
    @for_attribute("object.message.body")
    def get_body(self, message: dict) -> VkontakteTokens:
        """
        этот хендлер должен идти последним и отрабаывать в том
        случае, когда у сообщения нет пейлоада.
        Это текстовые сообщения из старых клиентов ВК.
        """
        return self._get_text_tokens(message)
