import attr


from .base_parser import AbstractTokens, BaseParser, for_attribute


@attr.s(auto_attribs=True)
class ViberTokens(AbstractTokens):
    deeplink_params: dict = attr.ib(factory=dict)

    # подпись к файлу (актуально для типа PICTURE, DOCUMENT, VIDEO)
    caption: str = attr.ib(default=None)

    viber_api_version: int = attr.ib(default=None)


class ViberParser(BaseParser):
    @staticmethod
    def _common_params(message: dict) -> dict:
        messenger_id = message["sender"]["id"]
        # вайбер может прислать сообщение без имени автора вообще
        author_first_name = message["sender"].get("name", "Anonymous")
        viber_api_version = message["sender"]["api_version"]

        return {
            "messenger_id": messenger_id,
            "author_first_name": author_first_name,
            "author_last_name": "",
            "viber_api_version": viber_api_version,
        }

    @for_attribute("message.type", "text")
    def text(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.TEXT,
            content=message["message"]["text"],
        )

    @for_attribute("message.type", "picture")
    def picture(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.PICTURE,
            content=message["message"]["media"],
            file_info={
                "size": message["message"].get("size"),
                "name": message["message"].get("file_name"),
            },
        )

    @for_attribute("message.type", "video")
    def video(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.VIDEO,
            content=message["message"]["media"],
            file_info={
                "size": message["message"]["size"],
                "name": message["message"].get("file_name"),
            },
        )

    @for_attribute("message.type", "contact")
    def contact(self, message: dict) -> ViberTokens:

        contact_info = message["message"]["contact"]

        # Проверяем что человек отправил именно свой контакт
        no_contact_name = "name" not in contact_info
        same_contact_name = (
            "name" in contact_info
            and "name" in message["sender"]
            and message["sender"]["name"] == contact_info["name"]
        )
        if no_contact_name or same_contact_name:
            # XXX: Даже если есть совпадение -
            # это не значит, что контакт - 100% этого же человека.
            # Человек с именем Петя может отправить контакт
            # другого человека с именем Петя,
            # и мы никак не сможем проверить, что это разные люди.
            is_own_contact = True
        else:
            is_own_contact = False

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.CONTACT,
            content=contact_info["phone_number"],
            type_specific_data={"is_own_contact": is_own_contact},
        )

    @for_attribute("message.type", "file")
    def file(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.DOCUMENT,
            content=message["message"]["media"],
            file_info={
                "size": message["message"]["size"],
                "name": message["message"].get("file_name"),
            },
        )

    @for_attribute("message.type", "location")
    def location(self, message: dict) -> ViberTokens:

        location_data = message["message"]["location"]
        lat, lon = location_data["lat"], location_data["lon"]

        return ViberTokens(  # type: ignore
            **self._common_params(message),
            type=ViberTokens.LOCATION,
            content=f"{lat},{lon}",
            type_specific_data={"latitude": lat, "longitude": lon},
        )

    @for_attribute("event", "subscribed")
    def subscribed(self, message: dict) -> ViberTokens:

        # здесь у message атрибут user, а не sender,
        # поэтому self._common_params неприменим.
        # Сообщение трактуем как текстовый /start.

        return ViberTokens(  # type: ignore
            type=ViberTokens.TEXT,
            content="/start",
            messenger_id=message["user"]["id"],
            author_first_name=message["user"].get("name", "Anonymous"),
            author_last_name="",
            viber_api_version=message["user"]["api_version"],
        )

    @for_attribute("event", "conversation_started")
    def conversation_started(self, message: dict) -> ViberTokens:

        context: str = message.get("context", "")

        return ViberTokens(  # type: ignore
            type=ViberTokens.CONVERSATION_STARTED,
            content="",
            messenger_id=message["user"]["id"],
            author_first_name=message["user"].get("name", "Anonymous"),
            author_last_name="",
            viber_api_version=message["user"]["api_version"],
            deeplink_params=BaseParser.parse_deeplink_params(context),
        )

    @for_attribute("event", "unsubscribed")
    def unsubscribed(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            type=ViberTokens.UNSUBSCRIBED,
            content="",
            messenger_id=message["user_id"],
            author_first_name="",
            author_last_name="",
            viber_api_version=1,
        )

    @for_attribute("event", "delivered")
    def delivered(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            type=ViberTokens.DELIVERED,
            content="",
            messenger_id=message["user_id"],
            author_first_name="",
            author_last_name="",
            viber_api_version=1,
        )

    @for_attribute("event", "seen")
    def seen(self, message: dict) -> ViberTokens:

        return ViberTokens(  # type: ignore
            type=ViberTokens.SEEN,
            content="",
            messenger_id=message["user_id"],
            author_first_name="",
            author_last_name="",
            viber_api_version=1,
        )
