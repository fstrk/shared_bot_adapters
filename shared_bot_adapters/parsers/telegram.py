import re
from mimetypes import guess_extension
from os.path import basename
from typing import Dict, Optional

import attr


from .base_parser import AbstractTokens, BaseParser, for_attribute


@attr.s(auto_attribs=True)
class TelegramTokens(AbstractTokens):

    # контекст, которая передается в виде "/start key1_value1"
    deeplink_params: dict = attr.ib(factory=dict)

    # если пользователь нажал инлайн-кнопку в сообщении -
    # то вместе с inline_telegram_msg_id нам приходит callback_query_id.
    # Если мы ответим на него методом answerCallbackQuery -
    # то пользователю в телеграмме вылезет алерт с сообщением.
    callback_query_id: str = attr.ib(default=None)

    # ID сообщения, в котором была нажата инлайн-кнопка Telegram
    # (позволяет при ответе подменить сообщение, а не отправлять новое)
    inline_telegram_msg_id: str = attr.ib(default=None)

    # внутренний ID файла в телеграмме. Изображения, видео и документы
    # прилетают с этим ID. По нему нужно восстанавливать реальный URL файла.
    telegram_file_id: str = attr.ib(default=None)

    # подпись к файлу (актуально для типа PICTURE, DOCUMENT, VIDEO)
    caption: str = attr.ib(default=None)

    # ID пользователя, который написал сообщение. Это не messenger_id:
    # messenger_id может быть как чатом 1к1, так и групчатом.
    # В переписке 1к1 messenger_id ==  author_id.
    # В групчате messenger_id != author_id
    author_id: str = attr.ib(default=None)


def _file_id_and_file_info(file_data: dict) -> dict:
    """ Универсальный метод получения инфы о файле: аудио, видео и тд"""
    file_info = {"size": file_data["file_size"]}
    return {"telegram_file_id": file_data["file_id"], "file_info": file_info}


def _alt_parse_deeplink_params(string: str) -> dict:
    """
    Альтернативный парсер диплинков для телеграмма,
    который использует другие разделители:
    одно подчеркивание и два подчеркивания, например,
        key1_value1__key2_value2.

    Потому что телеграм не понимает запятые и двоеточия в диплинках.

    """
    if not string:
        return {}
    _dict = {}
    pairs = [pair.strip() for pair in string.split("__")]

    for pair in pairs:

        search = re.search("(?P<key>.+)_(?P<value>.*)", pair)
        if search:
            _dict[search.group("key")] = search.group("value")
    return _dict


class TelegramParser(BaseParser):
    @staticmethod
    def _common_params(message: dict) -> dict:
        messenger_id = message["message"].get("chat", {}).get("id")
        message_from = message["message"]["from"]
        author_first_name = message_from["first_name"]
        author_last_name = message_from.get("last_name", "")
        author_id = message_from["id"]
        return {
            "messenger_id": str(messenger_id),
            "author_first_name": author_first_name,
            "author_last_name": author_last_name,
            "author_id": str(author_id),
        }

    @for_attribute("callback_query")
    def get_callback_query(self, message: dict) -> TelegramTokens:

        callback_query = message["callback_query"]

        return TelegramTokens(
            type=TelegramTokens.POSTBACK,
            content=callback_query["data"],
            callback_query_id=callback_query["id"],
            author_id=str(callback_query["from"]["id"]),
            inline_telegram_msg_id=callback_query["message"]["message_id"],
            messenger_id=str(callback_query["message"]["chat"]["id"]),
            author_first_name=callback_query["from"]["first_name"],
            author_last_name=callback_query["from"].get("last_name", ""),
        )

    @for_attribute("message.text")
    def get_text(self, message: dict) -> TelegramTokens:
        content = message["message"]["text"]

        deeplink_params: Dict[str, str] = {}
        if content.startswith("/start "):
            # если стартовая команда пришла с рефералом -
            # отрезаем реферала и кладем в отдельный атрибут
            deeplink_params_str = content[7:]
            deeplink_params = _alt_parse_deeplink_params(deeplink_params_str)
            content = "/start"

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            type=TelegramTokens.TEXT,
            content=content,
            deeplink_params=deeplink_params,
        )

    @for_attribute("message.photo")
    def get_photo(self, message: dict) -> TelegramTokens:

        photo_data = message["message"]["photo"].pop()

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            **_file_id_and_file_info(photo_data),
            type=TelegramTokens.PICTURE,
            content="<query this>",
            caption=message["message"].get("caption", ""),
        )

    @for_attribute("message.document")
    def get_document(self, message: dict) -> TelegramTokens:

        document_data = message["message"]["document"]

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            **_file_id_and_file_info(document_data),
            type=TelegramTokens.DOCUMENT,
            content="<query this>",
            caption=message["message"].get("caption", ""),
        )

    @for_attribute("message.location")
    def get_location(self, message: dict) -> TelegramTokens:

        lat = message["message"]["location"]["latitude"]
        lon = message["message"]["location"]["longitude"]
        content = f"{lat},{lon}"
        type_specific_data = {"latitude": lat, "longitude": lon}

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            type=TelegramTokens.LOCATION,
            content=content,
            type_specific_data=type_specific_data,
        )

    @for_attribute("message.contact")
    def get_contact(self, message: dict) -> TelegramTokens:

        common_params = self._common_params(message)

        content = message["message"]["contact"]["phone_number"]
        # иногда почему-то юзеры передают телефон с плюсом
        content = content.replace("+", "")

        user_id = message["message"]["contact"].get("user_id")
        type_specific_data = {
            "is_telegram_user": bool(user_id),
            "is_own_contact": str(user_id) == common_params["author_id"],
        }

        return TelegramTokens(  # type: ignore
            **common_params,
            type=TelegramTokens.CONTACT,
            content=content,
            type_specific_data=type_specific_data,
        )

    @for_attribute("message.audio")
    def get_audio(self, message: dict) -> TelegramTokens:

        audio_data = message["message"]["audio"]

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            **_file_id_and_file_info(audio_data),
            type=TelegramTokens.DOCUMENT,
            content="<query this>",
            caption=message["message"].get("caption", ""),
        )

    @for_attribute("message.video")
    def message_video(self, message: dict) -> TelegramTokens:
        video_data = message["message"]["video"]

        file_data = _file_id_and_file_info(video_data)

        telegram_file_id = file_data["telegram_file_id"]
        if video_data.get("file_path"):
            file_name = basename(video_data["file_path"])
        else:
            file_name = telegram_file_id + guess_extension(video_data["mime_type"])

        file_data["file_info"]["name"] = file_name

        return TelegramTokens(  # type: ignore
            **self._common_params(message),
            **_file_id_and_file_info(video_data),
            type=TelegramTokens.VIDEO,
            content="<query this>",
            caption=message["message"].get("caption", ""),
        )
