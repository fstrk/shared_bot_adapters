"""
Инструменты для создания классов-нормализаторов для разных платформ:
базовый класс и декоратор.


Все нормализаторы должны унаследоваться от BaseParser
и определить несколько обработчиков, декорированных @for_attribute.


Если в исходном словаре будет существовать атрибут,
определенный в @for_attribute, - то сработает соответствующий обработчик.


class SomeParser(BaseParser):

    ... кастомный конструктор ...

    @for_atribute('путь.к.нужному.атрибуту.сообщения', 'значение_атрибута')
    def handle_text_message(message):
        ... логика преобразования словаря с сообщением ...
        return ...

    если не нужно смотреть значение атрибута, а просто его наличие:

    @for_atribute('путь.к.нужному.атрибуту.сообщения')
    def handle_text_message(message):
        ... логика преобразования словаря с сообщением ...
        return ...


"""
import os
import re
from copy import copy
from typing import Callable, List, Optional, TypeVar
from urllib.parse import urlparse

import attr
from jsonpath_rw import parse

from shared_bot_adapters.storage import BaseCache

T = TypeVar("T")


class UnsupportedMessageError(Exception):
    """ Вызываем эту ошибку, когда не можем распарсить сообщение """

    pass


def get_filename_from_url(url: str) -> str:
    """ Возращает имя файла из URL. """
    return os.path.basename(urlparse(url).path)


def for_attribute(jsonpath_string: str, value: Optional[str] = None) -> Callable:
    """
    Декоратор, который каждому обернутому методу добавляет конфиг,
    позволяющий зарегистрировать метод как хендлер сообщения на классе
    """

    def register(method: T) -> T:

        if not hasattr(method, "parsers"):
            method.parsers = []  # type: ignore

        method.parsers.append(  # type: ignore
            {"parser": parse(jsonpath_string), "value": value}
        )

        return method

    return register


@attr.s(auto_attribs=True)
class AbstractTokens:
    """ обязательные для каждого ресивера токены """

    # Типы ботреквеста
    TEXT = "text"
    AUDIO = "audio"
    CONTACT = "contact"
    POSTBACK = "postback"
    LOCATION = "location"
    VIDEO = "video"
    CONVERSATION_STARTED = "conversation_started"
    PICTURE = "picture"
    DOCUMENT = "document"
    STICKER = "sticker"
    UNSUBSCRIBED = "unsubscribed"
    SEEN = "seen"
    DELIVERED = "delivered"

    EXISTING_TYPES = (
        TEXT,
        CONTACT,
        POSTBACK,
        LOCATION,
        AUDIO,
        VIDEO,
        CONVERSATION_STARTED,
        PICTURE,
        DOCUMENT,
        STICKER,
        UNSUBSCRIBED,
        SEEN,
        DELIVERED,
    )

    # тип сообщения: картинка, текст, файл и т.п.
    type: str

    # ID чата, из которого пришло сообщение
    messenger_id: str

    # first_name и last_name чата
    author_first_name: str
    author_last_name: Optional[str] = None

    # содержание сообщения: текст, URL картинки, геопозиция,
    # телефонный номер и пр.
    content: Optional[str] = None

    # словарь с данными о файле (если сообщение - файл).
    # {"name": ...., "size": ... }
    file_info: dict = attr.ib(factory=dict)

    # словарь с типоспецифичными данными (широта/долгота геопозиции и тд)
    type_specific_data: dict = attr.ib(factory=dict)

    def combined_name(self) -> str:
        name = self.author_first_name
        if self.author_last_name:
            name += " " + self.author_last_name
        return name


class OrderedMeta(type):
    """
    Метакласс, который ищет в объявляемом классе методы
    с атрибутами parser и value и складывает их в список message_handlers
    """

    def __new__(cls, clsname, bases, clsdict):

        if len(bases) > 1:
            raise Exception(
                f'Parser class "{clsname}" inherits from more than one class: '
                f"{bases}. This is not allowed; please inherit from one class "
                f"at most, because otherwise it would be hard to inherit "
                f"parser handlers."
            )

        klass = type.__new__(cls, clsname, bases, clsdict)

        if len(bases) > 0:
            # copy, чтобы список хенлеров не накапливался от класса к классу
            klass.message_handlers = copy(bases[0].message_handlers)
        else:
            klass.message_handlers = []

        for name, method in clsdict.items():
            if hasattr(method, "parsers"):
                for parser in method.parsers:
                    klass.message_handlers.append(
                        {
                            "parser": parser["parser"],
                            "value": parser["value"],
                            "handler": method,
                        }
                    )

        return klass


class BaseParser(metaclass=OrderedMeta):
    """
    Класс-основа всех нормализаторов.
    От него наследуются классы для индивидуальых платформ
    """

    message_handlers: List[dict]

    @staticmethod
    def parse_deeplink_params(string: str) -> dict:
        """
        Спарсить строку вида key:value,key:value и вернуть словарь
        {key:value, key:value}.
        Разрешены пустые value.
        """
        if not string:
            return {}
        _dict = {}
        pairs = [pair.strip() for pair in string.split(",")]

        for pair in pairs:
            search = re.search("(?P<key>.+):(?P<value>.*)", pair)
            if search:
                _dict[search.group("key")] = search.group("value")
        return _dict

    def parse(self, message: dict) -> AbstractTokens:
        """
        Проходим по всем обработчикам, зарегистрированным на классе,
        и если один из них нашел нужный ему атрибут - запускаем этот обработчик
        """

        for item in self.message_handlers:
            parser = item["parser"]
            result = parser.find(message)

            if not result:
                continue

            # если атрибут найден и нет ограничений на значение,
            # либо если ограничение есть и атрибут равен этому значению,
            # то выполняем хендлер
            if item["value"] is None or item["value"] == result[0].value:
                handler = item["handler"]
                return handler(self, message)

        # если ничего не подошло - вызываем ошибку
        raise UnsupportedMessageError(message)
