from typing import Union

from shared_bot_adapters.parsers.base_parser import AbstractTokens
from shared_bot_adapters.parsers.telegram import TelegramParser, TelegramTokens
from shared_bot_adapters.parsers.viber import ViberParser, ViberTokens
from shared_bot_adapters.parsers.vkontakte import VkontakteParser, VkontakteTokens

TELEGRAM = "telegram"
VKONTAKTE = "vkontakte"
VIBER = "viber"


parser_dict = {
    TELEGRAM: TelegramParser(),
    VIBER: ViberParser(),
    VKONTAKTE: VkontakteParser(),
}

#
# def _post_parse_hooks(self, tokens: AbstractTokens):
#     if isinstance(tokens, TelegramTokens):
#         TelegramQRDataPreparer(cache=self.cache).prepare_tokens(tokens)


def parse_message(message: dict, platform: str) -> AbstractTokens:
    parser = parser_dict[platform.lower()]
    tokens = parser.parse(message)
    return tokens
