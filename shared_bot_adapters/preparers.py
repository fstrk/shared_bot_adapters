from typing import Callable, Optional

from shared_bot_adapters.parsers.telegram import TelegramTokens
from shared_bot_adapters.parsers.vkontakte import VkontakteTokens
from shared_bot_adapters.state_objects import BotState


class TelegramQRDataPreparer:
    """
    Добавляем в QR-кнопки Telegram пейлоады.

    При инициализации необходим инджектить функции сохранния и доставания пейлоада:

    save_payload(text, data): ...
    get_payload(text): ...

    """

    def __init__(
        self,
        save_payload: Callable[[str, str], None],
        get_payload: Callable[[str], str],
    ) -> None:
        self.save_payload = save_payload
        self.get_payload = get_payload

    def prepare_state(self, state: BotState) -> Optional[BotState]:
        """
        Сохраняем пейлоады телеграмных QR-кнопок в Chat.telegram_qr_payloads.
        """

        if state.qr_buttons:
            for button in state.qr_buttons.get_buttons():
                if button.data:
                    self.save_payload(button.text, button.data)

        return state

    def prepare_tokens(self, tokens: TelegramTokens) -> Optional[TelegramTokens]:
        """
        Смотрит, что прилетело, и если это телеграмный ботреквест с текстовым
        контентом - пытается достать пейлоад из кэша
        """
        if tokens.content and tokens.type == tokens.TEXT:
            content = self.get_payload(tokens.content)
            if content:
                tokens.content = content
                tokens.type = tokens.POSTBACK

        return tokens


class VKDuplicateMessagePreparer:
    """
    Проверяет, не дубликат ли сообщения ВК к нам пришел (с апи ВК такое бывает).
    Если дубликат - не пропускает дальше tokens.

    При инициализации необходимо инжектить функции сохранения присланных токенов
     и проверки токенов на дубликаты:

    save_message(tokens): ...
    is_duplicate_message(tokens): ...

    """

    def __init__(
        self,
        save_message: Callable[[VkontakteTokens], None],
        is_duplicate_message: Callable[[VkontakteTokens], bool],
    ) -> None:
        self.save_message = save_message
        self.is_duplicate_message = is_duplicate_message

    def prepare_tokens(self, tokens: VkontakteTokens) -> Optional[VkontakteTokens]:
        """
        Смотрит, что прилетело, и если это телеграмный ботреквест с текстовым
        контентом - пытается достать пейлоад из кэша
        """
        if self.is_duplicate_message(tokens):
            return None
        self.save_message(tokens)
        return tokens
