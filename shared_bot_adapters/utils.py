from itertools import permutations
from typing import Iterable, Generator
from urllib.parse import urlparse, urlencode, parse_qsl, quote


def encode_query_params(url: str) -> str:
    """
    Энкодировать все GET-параметры строки, чтобы в ней был только один
    знак вопроса и было понятно, где начинаются и заканчиваются GET-параметры.

    Например, из http://a.b?url=http://backend.api?json=1
    сделать http://a.b?url=http%3A%2F%2Fbackend.api%3Fjson%3D1
    """
    parsed = urlparse(url)
    params = urlencode(parse_qsl(parsed.query), quote_via=quote)  # type: ignore
    restored_url = ""
    restored_url += parsed.scheme + "://"
    restored_url += parsed.netloc + parsed.path
    if parsed.params:
        restored_url += ";" + parsed.params
    if params:
        restored_url += "?" + params
    if parsed.fragment:
        restored_url += "#" + parsed.fragment
    return restored_url


def noquote(string, safe="", encoding=None, errors=None):
    return string


def flatten_irregular_list(irr_list: list) -> Generator:
    for item in irr_list:
        if isinstance(item, list):
            yield from flatten_irregular_list(item)
        else:
            yield item


def get_file_name(url: str) -> str:
    """
    Получить имя файла из URL
    """
    return url.split("/")[-1]
