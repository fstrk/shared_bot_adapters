from abc import ABC, abstractmethod
from typing import Optional, Dict, Any


class BaseCache(ABC):
    """
    Базовый класc кэша, от которого необходимо наследоваться при создании
    кэшей для VK сендера
    """

    @abstractmethod
    def get(self, key: str) -> Optional[str]:
        pass

    @abstractmethod
    def set(self, key: str, value: str) -> None:
        pass

    @abstractmethod
    def delete(self, key: str) -> None:
        pass


class InMemoryCache(BaseCache):
    def __init__(self) -> None:
        self.values: Dict[str, Any] = {}

    def get(self, key: str) -> Optional[str]:
        return self.values.get(key)

    def set(self, key: str, value: str) -> None:
        self.values[key] = value

    def delete(self, key: str) -> None:
        del self.values[key]
