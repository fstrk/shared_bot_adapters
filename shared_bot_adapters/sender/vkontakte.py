import json
import logging
import os
import tempfile
import time
from functools import wraps
from typing import List, Optional, Union, Any

from ..state_objects.vkontakte import (
    VKAudioMessage,
    VKCarouselMessage,
    VKDocumentMessage,
    VKImageMessage,
    VKMessage,
    VKQRButtonGroup,
    VKSleepMessage,
    VKTextMessage,
    VKVideoMessage,
)


from .adapters.vkontakte import VKApi, VkApiError, VkontakteUploader
from ..exceptions import SenderBotIsBlockedError
from ._base import BaseSender, handler_for
from ..storage import BaseCache

logger = logging.getLogger(__name__)


def transform_vkontakte_errors(method):
    """
    Обработчик ошибок при отправке VK, который
    преобразует несколько видов вк-ошибок в ошибки конструктора,
    которые позже обрабатываются в таске send_state.
    """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        try:
            return method(self, *args, **kwargs)
        except VkApiError as e:
            # Код 901 — означает, что пользователь запретил отправку сообщений
            # от сообщества
            if e.code == 901:
                raise SenderBotIsBlockedError(e.error_data["error_msg"])
            raise

    return wrapper


AnyMessageWithUrl = Union[
    VKImageMessage, VKAudioMessage, VKVideoMessage, VKDocumentMessage
]


class VkontakteSender(BaseSender):
    def __init__(
        self, page_api: VKApi, user_api: VKApi, *args: Any, **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)
        self.page_api = page_api
        self.user_api = user_api

    def _send_error(self, error_message, qr_buttons):
        """
        Отправить ошибку, упаковав его в текстовое сообщение.
        """
        message = VKTextMessage(text=error_message[:4096])
        return self._send_text_message(message, qr_buttons)

    def _send_only_qr_buttons(self, qr_buttons):
        raise NotImplementedError("VK API does not allow sending only QR buttons")

    def _get_attachment_ids(self, attachments: List[dict]) -> str:
        """
        Взять список словарей с вложениями (картинками, документами и пр),
        загрузить на сервера VK и вернуть их ID через строчку.
        :param attachments: список словарей в формате:
            [
                {"type": "image", "url": "..."},
                {"type": "document", "url": "..."},
                ...
            ]
        :return: строка идентификаторов "id_1,id_2,id_3,..."
        """
        # TODO: требуется рефактор.
        vk_uploader = VkontakteUploader(
            vk_page_api=self.page_api, vk_user_api=self.user_api, cache=self.cache
        )

        for attachment in attachments:
            if attachment["type"] == "location":
                continue
            attachment_id = vk_uploader.get_attachment_id(
                attachment_url=attachment["url"], vk_user_token=self.user_api.token
            )
            attachment["id"] = attachment_id

        return ",".join(a["id"] for a in attachments)

    def _transform_attachment_data(self, attachments):
        """
        Получает список словарей с вложениями:
        [
            {"type": "image", "url": "..."},
            {"type": "document", "url": "..."},
            {"type": "location", "lat": "...", "long": "..."},
            ...
        ]
        Отдает словарь с параметрами,
        которые можно будет присоединить к словарю текстового сообщения,
        готовящемуся к отправке в ВК:

        {
            "attachment": "img1,img2,img3,...",
            "lat": "..."
            "long": "..."
        }

        """
        data = {}
        # локацию можно вложить в сообщение только один раз.
        location_attachments = [m for m in attachments if m["type"] == "location"]
        if location_attachments:
            data["lat"] = location_attachments[0]["lat"]
            data["long"] = location_attachments[0]["long"]
        nonlocation_attachments = [m for m in attachments if m["type"] != "location"]
        if nonlocation_attachments:
            data["attachment"] = self._get_attachment_ids(nonlocation_attachments)
        return data

    def _create_payload(
        self, message_text: str, template: Optional[dict] = None
    ) -> dict:
        """ Подготовить основу для отправки в ВК. Основа - это текст (пусть пустой)"""
        return {
            "message": message_text,
            "user_id": self.chat_id,
            # чтобы не генерил лишние превью ссылок
            "dont_parse_links": 1,
            "template": template,
        }

    def _add_json_keyboard(self, payload: dict, qr_buttons: VKQRButtonGroup) -> dict:
        """ Дополнить основу для отправки в ВК клавиатурой, если она есть"""
        return {
            **payload,
            "json_keyboard": json.dumps(qr_buttons.api_dict(), ensure_ascii=False),
        }

    def _add_attachment_id(self, payload: dict, attachment_url: str) -> dict:
        """ Добавить в пейлоад ID вложения по урлу"""
        attachment_id = self._get_attachment_id(attachment_url=attachment_url)
        return {**payload, "attachment": attachment_id}

    def _get_attachment_id(self, attachment_url: str) -> str:
        vk_uploader = VkontakteUploader(
            vk_page_api=self.page_api, vk_user_api=self.user_api, cache=self.cache
        )
        attachment_id = vk_uploader.get_attachment_id(
            attachment_url=attachment_url, vk_user_token=self.user_api.token
        )
        return attachment_id

    @handler_for(VKMessage)
    @transform_vkontakte_errors
    def _send_general_message(self, message, qr_buttons):
        """
        Метод отправки сообщения, содержащего много вложений.
        штатной отправки сообщения и отправки ошибок.
        """
        # формируем текст сообщения, дополняем user_id
        # (понадобится для пакетного запроса)
        data = {
            "message": message.text,
            # чтобы не генерил лишние превью ссылок
            "dont_parse_links": 1,
        }
        # обогащаем клавиатурой
        if qr_buttons:
            data["json_keyboard"] = json.dumps(
                qr_buttons.api_dict(), ensure_ascii=False
            )

        attachment_data = self._transform_attachment_data(message.attachments)

        data.update(attachment_data)

        self.page_api.send_message(**data)

    @handler_for(VKImageMessage, VKAudioMessage, VKVideoMessage, VKDocumentMessage)
    @transform_vkontakte_errors
    def _send_attachment_message(
        self, message: AnyMessageWithUrl, qr_buttons: Optional[VKQRButtonGroup] = None
    ) -> None:
        """
        Отправить сообщение VK с одним вложением (картинкой, видео и тд).
        :param message: сообщение на отправку
        :param qr_buttons: кнопки
        """
        data = self._create_payload(message_text=message.caption or "")
        data = self._add_attachment_id(data, attachment_url=message.url)

        if qr_buttons:
            data = self._add_json_keyboard(data, qr_buttons)

        self.page_api.send_message(**data)

    @handler_for(VKTextMessage)
    @transform_vkontakte_errors
    def _send_text_message(
        self, message: VKTextMessage, qr_buttons: Optional[VKQRButtonGroup] = None
    ) -> None:
        """
        Отправить текстовое сообщение VK.
        :param message: сообщение на отправку
        :param qr_buttons: кнопки
        """
        data = self._create_payload(message_text=message.text)
        if qr_buttons:
            data = self._add_json_keyboard(data, qr_buttons)

        self.page_api.send_message(**data)

    @handler_for(VKSleepMessage)
    @transform_vkontakte_errors
    def _send_sleep_message(self, message, qr_buttons=None):
        if message.show_typing:
            self.page_api.send_typing(user_id=str(self.chat_id))
        time.sleep(10 if message.seconds > 10 else message.seconds)
        if message.seconds > 10:
            if message.show_typing:
                self.page_api.send_typing(user_id=str(self.chat_id))
            time.sleep(message.seconds - 10)
        else:
            time.sleep(message.seconds)
