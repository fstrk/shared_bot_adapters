import json
import logging
import time
from functools import wraps
from typing import Any, Union

from .adapters.telegram import (
    BotWasBlockedError,
    InlineKeyboardMarkup,
    ReadTimeoutError,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    TGApi,
    TGApiError,
    TooManyRequestsError,
)

from ..exceptions import SenderBotIsBlockedError, SendRequestError, StateObjectError
from ..state_objects import (
    TGAudioMessage,
    TGDocumentMessage,
    TGImageMessage,
    TGLocationMessage,
    TGQRButtonGroup,
    TGSleepMessage,
    TGTextMessage,
)
from ._base import BaseSender, handler_for

logger = logging.getLogger(__name__)


def transform_telegram_errors(method):
    """
    Обработчик ошибок при отправке Telegram, который
    преобразует несколько видов телеграм-ошибок в ошибки конструктора,
    которые позже обрабатываются в таске send_state.
    """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        try:
            return method(self, *args, **kwargs)
        except TooManyRequestsError as e:

            try:
                retry_after = e.json["parameters"]["retry_after"]
            except (KeyError, AttributeError):
                # По умолчанию пробуем повторить через 1 минуту
                retry_after = 60
            raise SendRequestError(retry_after=retry_after, detail=str(e))
        except ReadTimeoutError as e:
            # Если запрос оборвался, пробуем повторить через 10 секунд.
            raise SendRequestError(retry_after=10, detail=str(e))
        except BotWasBlockedError as e:
            raise SenderBotIsBlockedError(e)
        except TGApiError as e:
            # chat not found бывает, когда у бота меняешь токен на другого бота,
            # и он начинает слать пуши подписчикам старого бота,
            # которые на нового не подписывались.
            # user is deactivated - непонятно, откуда берется; наверное, если
            # пользователь удалился из Telegram.
            # bot can\'t initiate conversation with a user - тоже неясно
            known_error_messages = [
                "chat not found",
                "user is deactivated",
                "bot can't initiate conversation with a user",
            ]
            if any(
                map(
                    lambda string: string in e.json["description"].lower(),
                    known_error_messages,
                )
            ):
                raise SenderBotIsBlockedError(e)
            else:
                raise

    return wrapper


def generate_markup(
    qr_buttons: TGQRButtonGroup,
) -> Union[ReplyKeyboardMarkup, ReplyKeyboardRemove]:
    """
    Вернуть либо markup c новыми QR-кнопками,
    либо инструкцию спрятать старые QR-кнопки
    """
    if qr_buttons.buttons[0]:
        markup = ReplyKeyboardMarkup(
            keyboard=qr_buttons.api_dict(),
            resize_keyboard=True,
            one_time_keyboard=qr_buttons.one_time_keyboard,
        )
    elif qr_buttons.restore_normal_keyboard:
        markup = ReplyKeyboardRemove(hide_keyboard=True)
    else:
        msg_api_dict = json.dumps(qr_buttons.api_dict(), ensure_ascii=False)
        raise StateObjectError(
            f"not supposed to get here. Current structure: {msg_api_dict}"
        )
    return markup


def handle_message_markup(method):
    """
    Обработчик кнопок в Telegram, который делает две вещи:

    - перед отправки преобразует их в валидный markup для передачи в telepot
    - после отправки в зависимости от типа кнопок
        сохраняет сообщение в кэше чата, чтобы потом его можно было изменить
    """

    @wraps(method)
    def wrapper(self, message, qr_buttons):
        if message.inline_buttons:
            inline_buttons = message.inline_buttons.api_dict()
            qr_buttons = InlineKeyboardMarkup(inline_keyboard=inline_buttons)
        elif message.restore_qwerty_keyboard:
            qr_buttons = ReplyKeyboardRemove(hide_keyboard=True)
        elif qr_buttons:
            qr_buttons = generate_markup(qr_buttons)
        else:
            qr_buttons = None
        new_message_id = method(self, message, qr_buttons)

        # Можно изменять сообщения только с Inline кнопками, поэтому
        # только у них мы запоминаем ID отправленного сообщения
        # для последующих изменений
        # if type(qr_buttons) == InlineKeyboardMarkup:
        #     self.chat.add_markup_msg_id(new_message_id)

    return wrapper


class TelegramSender(BaseSender):
    def __init__(self, api: TGApi, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.api = api

    def _send_error(self, error_message, qr_buttons):
        """
        Отправить ошибку в текстовом сообщении
        """
        if isinstance(qr_buttons, TGQRButtonGroup):
            markup = generate_markup(qr_buttons)
        else:
            markup = None

        self.api.send_message(
            chat_id=self.chat_id, text=error_message, reply_markup=markup
        )

    def _send_only_qr_buttons(self, qr_buttons):
        raise NotImplementedError("Telegram API does not allow sending only QR buttons")

    @handler_for(TGSleepMessage)
    @transform_telegram_errors
    def _send_sleep_message(self, message, qr_buttons=None):
        if message.show_typing:
            self.api.send_typing(chat_id=self.chat_id)
        time.sleep(message.seconds)
        print(f"sending TGSleepMessage: {message}")

    def _send_with_noparse_fallback(self, method, **kwargs):
        """
        Выполнить метод, обработать исключение 'cant parse entities'
        (которое бывает, когда сообщение не изменено)
        и вернуть результирующее новое сообщение
        """
        try:
            res = method(**kwargs)
        except TGApiError as e:
            if "can't parse entities" in e.json["description"]:
                del kwargs["parse_mode"]
                res = method(**kwargs)
            else:
                raise
        return res["message_id"]

    @handler_for(TGTextMessage)
    @handle_message_markup
    @transform_telegram_errors
    def _send_tg_text_message(self, message, qr_buttons):
        """Отправить текстовое сообщение, вернуть его ID.

        :param message: сообщение типа TGTextMessage
        :param qr_buttons: markup, один из объектов библиотеки Telepot
        """

        # парсить ли форматирование Makrdown или нет?
        # (сообщения без парсинга используются
        # для передачи всякой служебной информации,
        # где подчеркивания и звездочки несут неоформительское значение)
        if message.formatted:
            parse_mode = "Markdown"
        else:
            parse_mode = None

        # Если нужно заменить какое-то из старых сообщений, то заменяем...
        # (только для текстовых сообщений c InlineKeyboardMarkup)
        if message.replace_telegram_msg and type(qr_buttons) in [
            InlineKeyboardMarkup,
            type(None),
        ]:
            # новое сообщение мы не создаем, поэтому ID сообщения
            # остается старым
            new_message_id = message.replace_telegram_msg
            try:
                params = {
                    "chat_id": self.chat_id,
                    "message_id": message.replace_telegram_msg,
                    "text": message.text,
                    "reply_markup": qr_buttons,
                    "parse_mode": parse_mode,
                    "disable_web_page_preview": not message.web_preview,
                }
                # отправляем сообщение с форматированием,
                # с фоллбеком на сообщение без форматирования

                self._send_with_noparse_fallback(self.api.edit_message_text, **params)

                return new_message_id
            # Игнорируем ошибки:
            # - если сообщение не изменено,
            # - если его невозможно редактировать
            # - или невозможно найти
            # - или это изображение-картинка, а не текст,
            #       и такие сообщения нельзя менять.
            # Эти варианты встречаются,
            # когда бота перекидывают с одной учетки Telegram на другую.
            except TGApiError as e:
                ignored_errors = [
                    "message is not modified",
                    "message to edit not found",
                    "message can't be edited",
                    "no text in the message to edit",
                ]
                desc = e.json["description"].lower()
                if not any(l in desc for l in ignored_errors):
                    raise

        # Если нужно создать новое (или если попытка редактировать не удалась),
        # создаем новое сообщение

        params = {
            "chat_id": self.chat_id,
            "text": message.text,
            "reply_markup": qr_buttons,
            "parse_mode": parse_mode,
            "disable_web_page_preview": not message.web_preview,
        }
        # отправляем сообщение с форматированием,
        # с фоллбеком на сообщение без форматирования
        new_message_id = self._send_with_noparse_fallback(
            self.api.send_message, **params
        )

        return new_message_id

    @handler_for(TGImageMessage)
    @handle_message_markup
    @transform_telegram_errors
    def _send_tg_image_message(self, message, qr_buttons):
        """Отправить изображение сообщение, вернуть его ID.

        :param message: сообщение типа TGImageMessage
        :param qr_buttons: markup, один из объектов библиотеки Telepot
        """
        response = self.api.send_photo(
            chat_id=self.chat_id,
            photo=message.url,
            caption=message.caption,
            reply_markup=qr_buttons,
            parse_mode="Markdown",
        )
        new_message_id = response["message_id"]
        return new_message_id

    @handler_for(TGLocationMessage)
    @handle_message_markup
    @transform_telegram_errors
    def _send_tg_location_message(self, message, qr_buttons):
        """Отправить локацию в сообщении, вернуть его ID.

        :param message: сообщение типа TGLocationMessage
        :param qr_buttons: markup, один из объектов библиотеки Telepot
        """

        response = self.api.send_location(
            chat_id=self.chat_id,
            latitude=message.latitude,
            longitude=message.longitude,
            reply_markup=qr_buttons,
        )
        new_message_id = response["message_id"]
        return new_message_id

    @handler_for(TGAudioMessage)
    @handle_message_markup
    @transform_telegram_errors
    def _send_tg_audio_message(self, message, qr_buttons):
        """Отправить аудио сообщение, вернуть его ID.

        :param message: сообщение типа TGIAudioMessage
        :param qr_buttons: markup, один из объектов библиотеки Telepot
        """
        response = self.api.send_voice(
            chat_id=self.chat_id,
            voice=message.url,
            caption=message.caption,
            reply_markup=qr_buttons,
        )

        new_message_id = response["message_id"]

        return new_message_id

    @handler_for(TGDocumentMessage)
    @handle_message_markup
    @transform_telegram_errors
    def _send_tg_document_message(self, message, qr_buttons):
        """Отправить документ, вернуть его ID.

        :param message: сообщение типа TGDocumentMessage
        :param qr_buttons: markup, один из объектов библиотеки Telepot
        """

        res = self.api.send_document(
            chat_id=self.chat_id,
            document=message.url,
            caption=message.caption,
            reply_markup=qr_buttons,
        )

        new_message_id = res["message_id"]
        return new_message_id
