import inspect
import logging
import time
from functools import wraps
from typing import (
    Any,
    Callable,
    Dict,
    Optional,
    Sequence,
    Type,
    cast,
    Union,
    Tuple,
    List,
)

import requests
from viberbot import Api
from viberbot.api.messages import FileMessage as NativeViberFileMessage
from viberbot.api.messages import PictureMessage as NativeViberPictureMessage
from viberbot.api.messages import RichMediaMessage
from viberbot.api.messages import TextMessage as NativeViberTextMessage

from ._base import BaseSender, handler_for
from ..exceptions import (
    SenderBadContentError,
    SenderBotIsBlockedError,
    SenderUserNotFoundError,
    SendRequestError,
    StateObjectError,
)
from ..state_objects import (
    BotState,
    VBAudioMessage,
    VBCarouselMessage,
    VBDocumentMessage,
    VBImageMessage,
    VBLocationMessage,
    VBQRButtonGroup,
    VBSleepMessage,
    VBTextMessage,
    VBVideoMessage,
)
from .adapters.viber import VBBotClient
from ..utils import get_file_name

logger = logging.getLogger(__name__)


def transform_viber_exceptions(method):
    """
    Декоратор, которым нужно оборачивать методы отправки вайбер-сообщений.
    Преобразует обычные Exception, которыми плюется вайберский клиент,
    в корректные SenderUserNotFoundError, SenderBotIsBlockedError и тд

    """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        try:
            return method(self, *args, **kwargs)
        except Exception as e:

            expected_errors = (
                "notSubscribed",
                "notRegistered",
                "receiverNoSuitableDevice",
                "failed with status: 999",
                "apiVersionNotSupported",
            )

            if any(x in str(e) for x in expected_errors):
                raise SenderBotIsBlockedError(e)
            else:
                msg = (
                    f"Error while sending viber message to {self.chat_id}: "
                    f"{e}. Args: {args}"
                )
                raise Exception(msg)

    return wrapper


class ViberSender(BaseSender):
    def __init__(
        self, viber_client: Api, api_version: int, *args: Any, **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self.api_version = api_version
        self.api = VBBotClient(api=viber_client)

    @staticmethod
    def _get_file_size_by_url(url: str) -> int:
        """
        Получить размер файла по URL (для отправки видео и документов в вайбер).
        Предварительно попытаться достать размер из кеша Redis
        """
        url_no_get_params = url.split("?")[0]
        response = requests.head(url)

        # по неведомой причине HEAD на амазоновские ресурсы
        # с подписанными урлами возвращает 403,
        # поэтому запрашиваем то же самое без подписи
        if response.status_code == requests.codes.forbidden:
            response = requests.head(url_no_get_params)

        # здесь невозможно сделать r.headers.get('last_modified'),
        # потому что headers - это не словарь, а сложный объект,
        # при обращении к которому через имя поля игнорируется кейс,
        # а при обращении через .get() - не игнорируется. Таким образом,
        # загловок Last-Modified большими буквами мы не поймаем через .get()
        try:
            fs = response.headers["Content-Length"]
            file_size = int(fs)
        except KeyError:
            file_size = 1024 * 1024  # просто мегабайт наугад

        return file_size

    @transform_viber_exceptions
    def _send_only_qr_buttons(self, qr_buttons: VBQRButtonGroup) -> int:
        """
        Отправить QR-клавиатуру без сообщений
        """
        return self.api.send_keyboard(
            to=self.chat_id,
            json_keyboard=qr_buttons.api_dict(),  # type: ignore
            min_api_version=self.api_version,
        )

    @handler_for(VBTextMessage)
    @transform_viber_exceptions
    def _send_vb_text_message(
        self, message: VBTextMessage, qr_buttons: Optional[VBQRButtonGroup]
    ) -> int:
        """
        Отправить текстовое сообщение Viber
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None
        return self.api.send_text(
            to=self.chat_id,
            text=message.text,
            json_keyboard=keyboard,
            min_api_version=self.api_version,
        )

    @handler_for(VBImageMessage)
    @transform_viber_exceptions
    def _send_vb_image_message(
        self, message: VBImageMessage, qr_buttons: Optional[VBQRButtonGroup]
    ) -> int:
        """
        Отправить сообщение-картинку Viber
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None
        return self.api.send_picture(
            to=self.chat_id,
            url=message.url,
            caption=message.caption,
            json_keyboard=keyboard,
            min_api_version=self.api_version,
        )

    @handler_for(VBVideoMessage)
    @transform_viber_exceptions
    def _send_vb_video_message(
        self, message: VBVideoMessage, qr_buttons: Optional[VBQRButtonGroup]
    ) -> int:
        """
        Отправить видеосообщение в Viber.
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None

        url = message.url
        if url is None:
            raise SenderBadContentError(
                f"Trying to send message without url: {message}"
            )
        file_size = message.size or self._get_file_size_by_url(url)

        return self.api.send_video(
            to=self.chat_id,
            url=url,
            size=file_size,
            duration=message.duration,
            caption=message.caption,
            json_keyboard=keyboard,
            min_api_version=self.api_version,
        )

    @handler_for(VBDocumentMessage, VBAudioMessage)
    @transform_viber_exceptions
    def _send_vb_document_message(
        self,
        message: Union[VBDocumentMessage, VBAudioMessage],
        qr_buttons: Optional[VBQRButtonGroup],
    ) -> int:
        """
        Отправить сообщение-документ Viber
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None
        url = message.url
        if url is None:
            raise SenderBadContentError(
                f"Trying to send message without url: {message}"
            )
        file_size = message.size or self._get_file_size_by_url(url)

        return self.api.send_file(
            to=self.chat_id,
            url=url,
            size=file_size,
            # убираем возможные GET-параметры из get_file_name
            file_name=get_file_name(url).split("?")[0],
            json_keyboard=keyboard,
            min_api_version=self.api_version,
        )

    @handler_for(VBCarouselMessage)
    @transform_viber_exceptions
    def _send_vb_carousel_message(
        self, message: VBCarouselMessage, qr_buttons: Optional[VBQRButtonGroup]
    ) -> Union[List[int], int]:
        """
        Отправить сообщение-карусель Viber.
        Здесь тонкость в том, что API вайбера не поддерживает одновременную
        отправку карусели и QR-кнопок, поэтому мы высылаем их
        двумя последовательными запросами.
        Может вернуть либо один идентификатор сообщения (если это только карусель),
        либо 2 (если это карусель + кнопки)
        """
        msg_ids = []
        if self.api_version == 1:
            # в случае, если версия вайбера 1
            # (например, если у пользователя Windows Phone),
            # отправляем текстовое сообщение.
            # Библиотека viberbot не умеет это делать.
            msg_id = self.api.send_text(
                to=self.chat_id,
                text=(
                    "Невозможно отобразить сообщение: "
                    "ваша версия Viber устарела. "
                    "Пожалуйста, обновите Viber."
                ),
                min_api_version=1,
            )
            msg_ids.append(msg_id)
        else:
            alt_text = (
                "Your Viber does not support this message. " "Please update your app."
            )
            msg_id = self.api.send_rich_media(
                to=self.chat_id, json_rich_media=message.api_dict(), alt_text=alt_text,
            )
            msg_ids.append(msg_id)

            if qr_buttons:
                msg_id = self._send_only_qr_buttons(qr_buttons)
                msg_ids.append(msg_id)
        return msg_ids

    @handler_for(VBLocationMessage)
    @transform_viber_exceptions
    def _send_vb_location_message(
        self, message: VBLocationMessage, qr_buttons: Optional[VBQRButtonGroup]
    ) -> int:
        """
        Отправить геолокацию в сообщении Viber
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None

        latitude, longitude = message.latitude, message.longitude
        if not latitude or not longitude:
            raise SenderBadContentError(
                f"Trying to send message without lat or lon: {message}"
            )

        return self.api.send_location(
            to=self.chat_id,
            latitude=str(latitude),
            longitude=str(longitude),
            json_keyboard=keyboard,
            min_api_version=self.api_version,
        )

    @handler_for(VBSleepMessage)
    def _send_vb_sleep_message(self, message, qr_buttons):
        """
        отправить паузу
        """
        time.sleep(message.seconds)

    def _send_error(  # type: ignore
        self, error_message: str, qr_buttons: Optional[VBQRButtonGroup]
    ) -> int:
        """
        Отправить ошибку в текстовом сообщении
        """
        keyboard = qr_buttons.api_dict() if qr_buttons else None
        return self.api.send_text(
            to=self.chat_id, text=error_message, json_keyboard=keyboard
        )


def get_viber_welcome_state_with_1st_msg(state: BotState, api_version: int) -> dict:
    """
    Сгенерировать приветственный стейт в формате, который понимает вайбер.
    Взять только первое сообщение из всех, которые будут в стейте.
    """

    # Делим сообщения на карусели и некарусели
    non_carousel_messages = [
        m for m in state.messages if not isinstance(m, VBCarouselMessage)
    ]
    carousel_messages = [m for m in state.messages if isinstance(m, VBCarouselMessage)]
    native_message_dict: Dict[str, Any] = {}

    # рисовать ли карусель? для этого должна подходить версия вайбера,
    # в стейте должна быть одна только карусель (без других сообщений)
    # и не должно быть клавиатуры
    render_carousel = (
        len(carousel_messages) == 1
        and len(non_carousel_messages) == 0
        and not state.qr_buttons
        and api_version >= 2
    )
    if render_carousel:
        native_message_dict = RichMediaMessage(
            rich_media=carousel_messages[0].api_dict(),
            alt_text="Your Viber does not support this message. "
            "Please update your app.",
            min_api_version=api_version,
        ).to_dict()
        return native_message_dict

    # если собрать карусель нельзя:
    # собираем первое не-карусельное сообщение
    if non_carousel_messages:
        message = non_carousel_messages[0]
        if isinstance(message, VBTextMessage):
            native_message_dict = NativeViberTextMessage(
                min_api_version=api_version, text=message.text
            ).to_dict()
        elif isinstance(message, VBImageMessage):
            native_message_dict = NativeViberPictureMessage(
                min_api_version=api_version, media=message.url, text=message.caption,
            ).to_dict()
        elif isinstance(message, VBDocumentMessage):
            native_message_dict = NativeViberFileMessage(
                min_api_version=api_version,
                media=message.url,
                size=10000,
                file_name=get_file_name(message.url) if message.url else "no name",
            ).to_dict()
        else:
            raise StateObjectError(
                "Tried to call get_viber_state_with_1st_msg, "
                "got unknown message type"
            )

    # если мы не собрали сообщение и у нас нет клавиатуры - отвечать нечем;
    # предупреждаем об ошибке.
    # Но только если версия вайбера позволяет это. Если у человека
    # вайбер версии 1 - мы уже ничего не можем поделать.
    if not native_message_dict and not state.qr_buttons and api_version >= 2:
        logger.error(
            "Trying to create Viber native state without message "
            "AND without keyboard. Please supply at least one of them: "
            "either message or keyboard."
        )

    # если же клавиатура есть - вставляем ее в сообщение.
    if isinstance(state.qr_buttons, VBQRButtonGroup):
        keyboard = state.qr_buttons.api_dict()
        native_message_dict["keyboard"] = keyboard

    return native_message_dict
