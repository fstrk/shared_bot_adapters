import functools
import logging
from typing import Callable, NamedTuple, Optional, Type, TypeVar, Union, Any
from urllib.parse import urlparse


import telepot
import urllib3
from telepot import namedtuple
from telepot.namedtuple import ForceReply, Message


logger = logging.getLogger(__name__)


ReplyKeyboardMarkup = namedtuple.ReplyKeyboardMarkup
ReplyKeyboardRemove = namedtuple.ReplyKeyboardRemove
InlineKeyboardMarkup = namedtuple.InlineKeyboardMarkup

REPLY_MARKUP = Union[
    namedtuple.InlineKeyboardMarkup,
    namedtuple.ReplyKeyboardMarkup,
    namedtuple.ReplyKeyboardRemove,
    ForceReply,
]
CHAT_ID = Union[int, str]


class MeInfo(NamedTuple):
    """
    Информация о пользователе.
    """

    id: int
    can_join_groups: bool
    can_read_all_group_messages: bool
    is_bot: bool
    first_name: str
    supports_inline_queries: bool
    username: str


class TGApiError(telepot.exception.TelegramError):
    """
    Ошибка при обращении к API Telegram
    """

    DESCRIPTION_PATTERNS: list = []


class TooManyRequestsError(TGApiError, telepot.exception.TooManyRequestsError):
    """
    Ошибка лимита запросов в АПИ.
    """


class ReadTimeoutError(urllib3.exceptions.ReadTimeoutError):
    """
    Ошибка долгих запросов.
    """


class BotWasBlockedError(TGApiError, telepot.exception.BotWasBlockedError):
    """
    Бот был заблокирован пользователем.
    """


def transform_exception(func):
    """
    Перехватывает ошибки от библиотеки и кидает
    переопределенные TGApiError
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (
            telepot.exception.TelegramError,
            urllib3.exceptions.ReadTimeoutError,
        ) as e:

            tb = e.__traceback__

            if isinstance(e, telepot.exception.TelegramError):

                kwargs = dict(
                    description=e.description, error_code=e.error_code, json=e.json
                )
                if isinstance(e, telepot.exception.BotWasBlockedError):
                    exc = BotWasBlockedError(**kwargs)
                elif isinstance(e, telepot.exception.TooManyRequestsError):
                    exc = TooManyRequestsError(**kwargs)
                else:
                    exc = TGApiError(**kwargs)

            elif isinstance(e, urllib3.exceptions.ReadTimeoutError):
                exc = ReadTimeoutError(pool=e.pool, url=e.url, message=str(e))
            else:
                exc = e

            raise exc.with_traceback(tb) from None

    return wrapper


DataType = TypeVar("DataType")


def to_object(class_type: Type[DataType]) -> Callable:
    """
    Превращает данные ответа в соответствующий тип данных.
    """

    def container(func: Callable[..., dict]) -> Callable[..., DataType]:
        @functools.wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> DataType:
            response = func(*args, **kwargs)
            data = class_type(**response)  # type: ignore
            return data

        return wrapper

    return container


class TGApi:
    """
    Клиент для доступа к API Телеграма
    """

    def __init__(self, token: str):
        self.token = token
        self.client = telepot.Bot(token=token)

    @transform_exception
    def set_webhook(self, webhook_url: str) -> None:
        """
        Установить URL адрес для приема Вебхуков от Telegram
        """
        logger.debug(f"TGApi: setting webhook {webhook_url}.")
        self.client.setWebhook(url=webhook_url)

    @transform_exception
    def delete_webhook(self) -> None:
        """
        Удалить подключенный адрес для вебхука.
        """
        logger.debug(f"TGApi: deleting webhook.")
        self.client.deleteWebhook()

    @to_object(class_type=MeInfo)
    @transform_exception
    def get_me(self) -> MeInfo:
        """
        Получить информацию о клиенте.
        """
        response = self.client.getMe()
        logger.info(f"Bot info: {response}")
        return response

    def get_username(self) -> str:
        """
        Получить имя текущего пользователя
        """
        info = self.get_me()
        return info.username

    @transform_exception
    def _get_chat(self, chat_id: CHAT_ID) -> dict:
        """
        Информация о чате.

        TODO: если потребуется сделать публичным, то следует обернуть в декоратор
            to_object и написать класс объекта.
        """
        return self.client.getChat(chat_id=chat_id)

    def get_chat_avatar_url(self, chat_id: CHAT_ID) -> Optional[str]:
        """
        Ссылка на файл аватара.
        """
        chat = self._get_chat(chat_id)
        file_id = chat.get("photo", {}).get("small_file_id")
        if file_id:
            return self.get_file_url(file_id)
        return None

    @transform_exception
    def send_message(
        self,
        chat_id: CHAT_ID,
        text: str,
        parse_mode: Optional[str] = None,
        disable_web_page_preview: bool = False,
        reply_markup: Optional[REPLY_MARKUP] = None,
    ) -> Message:
        """
        Отправить сообщение
        Args:
            chat_id: ID подписчика
            text: текст сообщения
            parse_mode: тип контента сообщения (Markdown or HTML)
            disable_web_page_preview: отключить превью для ссылок
            reply_markup: доп инструкции для интерфейса (клавиатуры)
        Returns:
            Сообщение телеграм
        """
        return self.client.sendMessage(
            chat_id=chat_id,
            text=text,
            parse_mode=parse_mode,
            disable_web_page_preview=disable_web_page_preview,
            reply_markup=reply_markup,
        )

    @transform_exception
    def send_photo(
        self,
        chat_id: CHAT_ID,
        photo: str,
        caption: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
        parse_mode: Optional[str] = None,
    ) -> Message:
        """
        Отправить фотографию.
        Args:
            chat_id: ID подписчика
            photo: ссылка на фотографию или идентификатор фото в телеграм
            caption: подпись к фотографии
            reply_markup: доп инструкции для интерфейса (клавиатуры)
            parse_mode: тип контента сообщения (Markdown or HTML)
        Returns:
            Сообщение телеграм
        """
        return self.client.sendPhoto(
            chat_id=chat_id,
            photo=photo,
            caption=caption,
            reply_markup=reply_markup,
            parse_mode=parse_mode,
        )

    @transform_exception
    def send_video(
        self,
        chat_id: CHAT_ID,
        video: str,
        caption: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
        parse_mode: Optional[str] = None,
    ) -> Message:
        """
        Отправить фотографию.
        Args:
            chat_id: ID подписчика
            video: ссылка на видео или идентификатор видео в телеграм
            caption: подпись к фотографии
            reply_markup: доп инструкции для интерфейса (клавиатуры)
            parse_mode: тип контента сообщения (Markdown or HTML)
        Returns:
            Сообщение телеграм
        """
        return self.client.sendVideo(
            chat_id=chat_id,
            video=video,
            caption=caption,
            reply_markup=reply_markup,
            parse_mode=parse_mode,
        )

    @transform_exception
    def send_voice(
        self,
        chat_id: CHAT_ID,
        voice: str,
        caption: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
        parse_mode: Optional[str] = None,
    ) -> Message:
        """
        Отправить аудиозапись
        Args:
            chat_id: ID подписчика
            audio: ссылка на аудиофайл или идентификатор аудио в телеграм
            caption: подпись к аудио
            reply_markup: доп инструкции для интерфейса (клавиатуры)
            parse_mode: тип контента сообщения (Markdown or HTML)
        Returns:
            Сообщение телеграм
        """
        return self.client.sendVoice(
            chat_id=chat_id,
            voice=voice,
            caption=caption,
            reply_markup=reply_markup,
            parse_mode=parse_mode,
        )

    @transform_exception
    def send_document(
        self,
        chat_id: CHAT_ID,
        document: str,
        caption: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
        parse_mode: Optional[str] = None,
    ) -> Message:
        """
        Отправить документ
        Args:
            chat_id: ID подписчика
            document: ссылка на документ или идентификатор документа в телеграм
            caption: подпись к аудио
            reply_markup: доп инструкции для интерфейса (клавиатуры)
            parse_mode: тип контента сообщения (Markdown or HTML)
        Returns:
            Сообщение телеграм
        """
        return self.client.sendDocument(
            chat_id=chat_id,
            document=document,
            caption=caption,
            reply_markup=reply_markup,
            parse_mode=parse_mode,
        )

    @transform_exception
    def send_location(
        self,
        chat_id: CHAT_ID,
        latitude: float,
        longitude: float,
        reply_markup: Optional[REPLY_MARKUP] = None,
    ) -> Message:
        """
        Отправить документ
        Args:
            chat_id: ID подписчика
            latitude: широта
            longitude: долгота
            reply_markup: доп инструкции для интерфейса (клавиатуры)

        Returns:
            Сообщение телеграм
        """
        return self.client.sendLocation(
            chat_id=chat_id,
            latitude=latitude,
            longitude=longitude,
            reply_markup=reply_markup,
        )

    @transform_exception
    def send_chat_action(self, chat_id: CHAT_ID, action: str) -> Message:
        """
        Отправить действие.
        Статус устанавливается на 5 секунд или меньше

        https://core.telegram.org/bots/api#sendchataction

        Args:
            chat_id: ID подписчика
            action: тип действия.
        """
        return self.client.sendChatAction(chat_id=chat_id, action=action)

    def send_typing(self, chat_id: CHAT_ID) -> Message:
        """
        Отправить событие о наборе сообщения
        """
        return self.send_chat_action(chat_id=chat_id, action="typing")

    @transform_exception
    def answer_callback_query(
        self, query_id: str, text: Optional[str] = None, show_alert: bool = False
    ) -> Message:
        """
        Ответ на CallbackQuery

        Args:
            query_id: идентификатор запроса
            text: сообщение, если пусто - ничего не будет отображено.
            show_alert: клиенту будет показано предупреждение
                вместо уведомления в верхней части экрана чата.
        """
        return self.client.answerCallbackQuery(
            callback_query_id=query_id, text=text, show_alert=show_alert
        )

    @transform_exception
    def edit_message_text(
        self,
        text: str,
        chat_id: Optional[CHAT_ID] = None,
        message_id: Optional[int] = None,
        inline_message_id: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
        parse_mode: Optional[str] = None,
        disable_web_page_preview: bool = False,
    ) -> Message:
        """
        Отредактировать отправленное сообщение.
        """
        return self.client.editMessageText(
            msg_identifier=inline_message_id or (chat_id, message_id),
            text=text,
            reply_markup=reply_markup,
            parse_mode=parse_mode,
            disable_web_page_preview=disable_web_page_preview,
        )

    @transform_exception
    def edit_message_reply_markup(
        self,
        chat_id: Optional[CHAT_ID] = None,
        message_id: Optional[int] = None,
        inline_message_id: Optional[str] = None,
        reply_markup: Optional[REPLY_MARKUP] = None,
    ) -> Message:
        """
        Изменить опции интерфейса бота (клавиатуры)
        """
        return self.client.editMessageReplyMarkup(
            msg_identifier=inline_message_id or (chat_id, message_id),
            reply_markup=reply_markup,
        )

    @transform_exception
    def get_file(self, file_id: str) -> namedtuple.File:
        """
        Получить объект файла от телеграма.
        """
        return self.client.getFile(file_id=file_id)

    def get_file_path(self, file_id: str) -> str:
        """
        Путь до файла на сервере телеграм
        """
        data = self.get_file(file_id=file_id)

        return data["file_path"]

    def get_file_url(self, file_id: str) -> str:
        """
        Сформировать ссылку на файл, с учетом зеркала.
        """
        file_path = self.get_file_path(file_id=file_id)
        public_url = f"https://api.telegram.org/file/bot{self.token}/{file_path}"
        return public_url

    @staticmethod
    def get_deeplink(username: str, **context: dict,) -> str:
        """
        Ссылка на бота.

        Протокол - может быть tg http https.

        Возможно сделать username необязательным параметром и брать данные из
        API, когда и если такое потребуется.
        """
        ctx = "__".join([f"{k}_{v}" for k, v in context.items()])
        ctx = f"start={ctx}" if ctx else ""

        link = f"tg://resolve?domain={username}"
        ctx = f"&{ctx}" if ctx else ""
        return f"{link}{ctx}"
