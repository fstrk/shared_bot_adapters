import functools
import logging
from typing import List, NamedTuple, Optional, Union

from viberbot import Api, BotConfiguration
from viberbot.api.messages import (
    ContactMessage,
    FileMessage,
    KeyboardMessage,
    LocationMessage,
    PictureMessage,
    RichMediaMessage,
    TextMessage,
    VideoMessage,
)
from viberbot.api.messages.data_types.contact import Contact
from viberbot.api.messages.data_types.location import Location
from viberbot.api.messages.message import Message

logger = logging.getLogger(__name__)

API_VERSION = Optional[Union[int, float]]


class ViberAPIError(Exception):
    """
    Ошибка при обращении к API Viber.
    """

    def __init__(self, message: str, code: Optional[int] = None):
        self.message = message
        self.code = code

    def __str__(self) -> str:
        detail = f"{self.__class__.__name__}"
        if self.code is not None:
            detail += f" #{self.code}"
        detail += f":{self.message}"
        return detail


def transform_exception(func):
    """
    Перехватывает ошибки от библиотеки и кидает
    переопределенные ViberAPIError

    Про ошибки можно почитать здесь:
    https://developers.viber.com/docs/api/rest-bot-api/#error-codes
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as e:
            # Библиотека не содержит собственных ошибок.
            tb = e.__traceback__
            raise ViberAPIError(str(e)).with_traceback(tb) from None

        if not isinstance(result, dict) or "status" not in result:
            # В таком случае мы не можем точно знать, что это ошибка.
            return result

        status = result.get("status")
        if status != 0:
            message = result.get("status_message")
            raise ViberAPIError(message, code=status)
        return result

    return wrapper


class AccountInfo(NamedTuple):
    """
    Информация об аккаунте.
    """

    name: str
    uri: str
    icon: str
    category: str
    subcategory: str
    country: str
    webhook: str


class VBBotClient:
    """
    Фасад для работы с Viber Bot API через библиотеку.
    """

    DELIVERED = "delivered"
    SEEN = "seen"
    SUBSCRIBED = "subscribed"
    UNSUBSCRIBED = "unsubscribed"
    CONVERSATION_STARTED = "conversation_started"

    def __init__(self, api: Api, webhook_events: Optional[List[str]] = None):
        self.api = api
        self.webhook_events = webhook_events or [
            self.SUBSCRIBED,
            self.UNSUBSCRIBED,
            self.CONVERSATION_STARTED,
        ]

    @transform_exception
    def set_webhook(self, url: str) -> None:
        """
        Установить вебхук
        """
        self.api.set_webhook(url=url, webhook_events=self.webhook_events)

    @transform_exception
    def delete_webhook(self) -> None:
        """
        Удалить вебхук.
        """
        self.api.unset_webhook()

    @transform_exception
    def get_account_info(self) -> AccountInfo:
        """
        Получить информацию об аккаунте.

        NOTE: Пока ничего не возвращает, так как нам не важна эта информация
        """
        result = self.api.get_account_info()

        return AccountInfo(
            name=result["name"],
            uri=result["uri"],
            icon=result["icon"],
            category=result["category"],
            subcategory=result["subcategory"],
            country=result["country"],
            webhook=result["webhook"],
        )

    def get_account_uri(self) -> str:
        """
        Получить информацию о URI.
        """
        info = self.get_account_info()
        return info.uri

    @transform_exception
    def _send_messages(self, to: str, messages: List[Message]) -> List[int]:
        """
        Отправляет сообщения подписчику.

        Вернет список идентификаторов сообщений.
        """
        return self.api.send_messages(to=to, messages=messages)

    def _send_message(self, to: str, message: Message) -> int:
        """
        Отправляет 1 сообщение подписчику.

        Вернет идентификатор сообщения.
        """
        return self._send_messages(to=to, messages=[message])[0]

    def send_text(
        self,
        to: str,
        text: str,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить текстовое сообщение подписчику.

        Args:
            to: идентификатор подписчика.
            text: текст сообщения
            json_keyboard: Клавиатура, прикрепленная к сообщению.
            min_api_version: минимальная версия для получения сообщения.

        Returns:
            Идентификатор сообщения
        """

        return self._send_message(
            to=to,
            message=TextMessage(
                text=text, keyboard=json_keyboard, min_api_version=min_api_version
            ),
        )

    def send_picture(
        self,
        to: str,
        url: Optional[str] = None,
        caption: Optional[str] = None,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить картинку
        Args:
            to: ID подписчика
            url: URL картинки
            caption: капча для картинки
            json_keyboard: клавиатура к сообщению
            min_api_version: минимальная версия API.

        Returns:
            Идентификатор сообщения
        """
        return self._send_message(
            to=to,
            message=PictureMessage(
                media=url,
                text=caption,
                keyboard=json_keyboard,
                min_api_version=min_api_version,
            ),
        )

    def send_video(
        self,
        to: str,
        url: str,
        size: int,
        duration: Optional[int] = None,
        caption: Optional[str] = None,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить видео

        Args:
            to: ID подписчика
            url: URL видео
            size: размер видео файла
            duration: длительность видео, будет указана под видео. <= 180 сек.
            caption: подпись под видео
            json_keyboard: клавиатура
            min_api_version: минимальная версия API.

        Returns:
            Идентификатор сообщения
        """
        return self._send_message(
            to=to,
            message=VideoMessage(
                media=url,
                size=size,
                duration=duration,
                text=caption,
                keyboard=json_keyboard,
                min_api_version=min_api_version,
            ),
        )

    def send_file(
        self,
        to: str,
        url: str,
        size: int,
        file_name: str,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить файл.
        Args:
            to: ID подписчика
            url: URL адрес файла
            size: размер файла, максимум 50 мб.
            file_name: Имя файла, максимум 256 символов
            json_keyboard: клавиатура.
            min_api_version: версия API.

        Returns:
            ID сообщения.
        """
        return self._send_message(
            to=to,
            message=FileMessage(
                media=url,
                size=size,
                file_name=file_name,
                keyboard=json_keyboard,
                min_api_version=min_api_version,
            ),
        )

    def send_location(
        self,
        to: str,
        latitude: str,
        longitude: str,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить локацию.

        Args:
            to: ID подписчика
            latitude: широта
            longitude: долгота
            json_keyboard: клавиатура
            min_api_version: минимальная версия API.

        Returns:
            ID сообщения
        """

        loc = Location(lat=latitude, lon=longitude)

        return self._send_message(
            to=to,
            message=LocationMessage(
                keyboard=json_keyboard, location=loc, min_api_version=min_api_version
            ),
        )

    def send_rich_media(
        self,
        to: str,
        json_rich_media: dict,
        alt_text: Optional[str] = None,
        json_keyboard: Optional[dict] = None,
        min_api_version: API_VERSION = 3,
    ) -> int:
        """
        Отправить сообщение-карусель.

        Поддерживаются девайсы с версией 6.7 и выше.
        Args:
            to: ID подписчика.
            json_rich_media: JSON представление карусели.
            alt_text: сообщение, в случае,
                если контент не поддерживается версией.
            json_keyboard: клавиатура
            min_api_version: минимальная версия API.

        Returns:
            ID сообщения.
        """
        return self._send_message(
            to=to,
            message=RichMediaMessage(
                rich_media=json_rich_media,
                alt_text=alt_text,
                keyboard=json_keyboard,
                min_api_version=min_api_version,
            ),
        )

    def send_keyboard(
        self,
        to: str,
        json_keyboard: dict,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить клавиатуру.
        Args:
            to: ID подписчика
            json_keyboard: JSO представление клавиатуры
            min_api_version: минимальная версия API.
        Returns:
            ID сообщения.
        """
        return self._send_message(
            to=to,
            message=KeyboardMessage(
                keyboard=json_keyboard, min_api_version=min_api_version
            ),
        )

    def send_contact(
        self,
        to: str,
        name: str,
        phone_number: str,
        json_keyboard: Optional[dict] = None,
        min_api_version: Optional[API_VERSION] = None,
    ) -> int:
        """
        Отправить контакт
        Args:
            to: ID подписчика
            name: имя контакта. <= 28 символов.
            phone_number: телефон контакта <= 18 символов
            json_keyboard: клавиатура
            min_api_version: минимальная версия API.

        Returns:
            ID сообщения
        """
        return self._send_message(
            to=to,
            message=ContactMessage(
                contact=Contact(name=name, phone_number=phone_number),
                keyboard=json_keyboard,
                min_api_version=min_api_version,
            ),
        )

    @staticmethod
    def get_deeplink(uri: str, **context: dict) -> str:
        """
        Ссылка для бота
        """

        ctx = ",".join([f"{k}:{v}" for k, v in context.items()])
        ctx = f"context={ctx}" if ctx else ""

        link = f"viber://pa?chatURI={uri}"
        ctx = f"&{ctx}" if ctx else ""
        return f"{link}{ctx}"
