import functools
import json
import logging
import random
from dataclasses import dataclass
from datetime import datetime
from typing import Iterable, List, NamedTuple, Optional, Union, Tuple, Any
import mimetypes
import os
from io import BytesIO
from urllib.parse import urlparse


import requests
import vk
from vk import exceptions

from shared_bot_adapters.storage import BaseCache

logger = logging.getLogger(__name__)


class VKAuthorizeData(NamedTuple):
    """
    Авторизационные данные для приложения VK
    """

    access_token: str
    user_id: Optional[str] = None
    group_id: Optional[str] = None

    @property
    def is_group(self) -> bool:
        """
        Означает, что данные для авторизации группы.
        """
        return bool(self.group_id)


class CallbackServer(NamedTuple):
    """
    Подключенный к странице сервер.
    """

    id: int
    url: str


class VkApiError(exceptions.VkAPIError):
    """
    Ошибка при обращении к API Vk
    """


def transform_exception(func):
    """
    Перехватывает ошибки от библиотеки и кидает
    переопределенные VkAPIError
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except exceptions.VkAPIError as e:
            tb = e.__traceback__
            raise VkApiError(error_data=e.error_data).with_traceback(tb) from None

    return wrapper


@dataclass
class VKAppConfig:
    client_id: int
    client_secret: str
    callback_server_name: str
    mini_app_id: Optional[str] = None
    mini_app_secret: Optional[str] = None


class VKApi:
    """
    Клиент до API Вконтакте
    """

    version = "5.103"
    servers_limit = 10

    # типы загрузки файлов в АПИ ВК.
    # (Их апи требует загружать и доки, и аудио на одну ручку, но с разными флагами)
    DOC = "DOC"
    AUDIO = "AUDIO"
    DOC_UPLOAD_TYPES = (DOC, AUDIO)

    def __init__(
        self,
        token: str,
        config: VKAppConfig,
        group_id: Optional[int] = None,
        user_id: Optional[int] = None,
    ) -> None:
        """

        :param token: токен доступа к API, полученный от имени юзера или группы
        :param group_id: если API инициализируется от группы - сюда передаем ID группы
        :param user_id: если API инициализируетс от юзера - передаем ID юзеа
        """
        self.config = config
        self.token = token
        self.group_id = group_id
        self.user_id = user_id
        self.session = vk.Session(access_token=self.token)
        self.api = vk.API(self.session, v=self.version)

    @transform_exception
    def add_callback_server(self, callback_url: str) -> str:
        """
        Установить адрес для вебхуков.
        """
        response = self.api.groups.addCallbackServer(
            group_id=self.group_id,
            url=callback_url,
            title=self.config.callback_server_name,
        )
        return response["server_id"]

    @transform_exception
    def set_callback_settings(self, server_id: str) -> None:
        """
        Подключить сервер.
        """
        self.api.groups.setCallbackSettings(
            group_id=self.group_id,
            server_id=server_id,
            api_version=self.version,
            message_new=1,
        )

    def update_webhook(self, callback_url: str) -> None:
        """
        Подключить страницу.
        """
        servers = self.get_callback_servers()

        connected = self.check_callback_server_connected(
            servers=servers, callback_url=callback_url
        )

        if not connected:
            self.raise_limit_callback_servers(servers=servers)

            server_id = self.add_callback_server(callback_url=callback_url)
            self.set_callback_settings(server_id=server_id)

    @transform_exception
    def get_callback_servers(self) -> List[CallbackServer]:
        """
        Получает список подключенных серверов.
        """
        assert self.group_id, "Разрешено только для API групп VK"

        servers = self.api.groups.getCallbackServers(group_id=self.group_id)
        items = servers["items"]
        return [CallbackServer(id=i["id"], url=i["url"]) for i in items]

    @transform_exception
    def delete_callback_server(self, server_id: int) -> None:
        """
        Отключить сервер от страницы.
        """
        assert self.group_id, "Разрешено только для API групп VK"

        self.api.groups.deleteCallbackServer(
            group_id=self.group_id, server_id=server_id
        )

    def delete_webhook(self, callback_url: str) -> None:
        """
        Удалить вебхуки от сервера.
        """
        connected_servers = self.get_callback_servers()

        for server in connected_servers:
            if callback_url in server.url:
                self.delete_callback_server(server_id=server.id)

    @transform_exception
    def get_callback_confirmation_code(self) -> str:
        """
        Получить подтверждающий код.
        """
        assert self.group_id, "Разрешено только для API групп VK"

        response = self.api.groups.getCallbackConfirmationCode(group_id=self.group_id)
        return response["code"]

    def check_callback_server_connected(
        self, callback_url: str, servers: Optional[List[CallbackServer]] = None
    ) -> bool:
        """
        Проверяет, подключен ли уже сервер к приложению
        """

        if servers is None:
            servers = self.get_callback_servers()

        for server in servers:
            if server.url == callback_url:
                return True
        else:
            return False

    def get_confirmation_response(self, callback_url: str) -> "str":
        """
        Подтвердить получение вебхука.
        Следует выполнять при получении запроса с type=confirmation
        """
        connected = self.check_callback_server_connected(callback_url=callback_url)

        if not connected:
            return ""

        return self.get_callback_confirmation_code()

    @transform_exception
    def send_message(
        self,
        user_id: int,
        message: Optional[str] = None,
        lat: Optional[float] = None,
        long: Optional[float] = None,
        attachment: Optional[str] = None,
        dont_parse_links: int = 1,
        json_keyboard: Optional[dict] = None,
        template: Optional[dict] = None,
    ) -> None:
        """
        Отправить сообщение подписчику
        Args:
            user_id: ид подписчика
            message: текст сообщения, обязателен, если не задан attachment
            lat: широта
            long: долгота
            attachment: вложения
            dont_parse_links: 1- не создавать сниппет ссылки из сообщения
            json_keyboard: клавиатура
            template: дополнительные сообщения (например карусели)
        """
        random_id = int(
            str(int(datetime.now().timestamp() * 1000)) + str(random.randint(100, 999))
        )
        template_json: Optional[str]
        if template is not None:
            template_json = json.dumps(template)
        else:
            template_json = None

        self.api.messages.send(
            user_id=user_id,
            message=message,
            lat=lat,
            long=long,
            random_id=random_id,
            attachment=attachment,
            dont_parse_links=dont_parse_links,
            keyboard=json_keyboard,
            template=template_json,
        )

    @transform_exception
    def send_activity(self, user_id: str, action_type: str) -> None:
        """
        Отправить событие об активности.
        """
        self.api.messages.setActivity(user_id=user_id, type=action_type)

    def send_typing(self, user_id: str) -> None:
        """
        Отправить "печатает сообщение".
        """
        self.send_activity(user_id=user_id, action_type="typing")

    @transform_exception
    def get_users(self, user_ids: Optional[Union[Iterable, str]] = None) -> list:
        """
        Список подключенных пользователей.
        """
        return self.api.users.get(user_ids=user_ids, fields=["photo_100"])

    def get_user_info(self, user_id: str) -> Optional[dict]:
        """
        Получить информацию о пользователе.
        """
        try:
            return self.get_users(user_ids=user_id)[0]
        except KeyError:
            return None

    def get_user_avatar_url(self, user_id: str) -> Optional[str]:
        info = self.get_user_info(user_id=user_id)
        if info:
            return info.get("photo_100")
        return None

    @transform_exception
    def get_photo_upload_server(self) -> dict:
        """
        Возвращает данные сервера для загрузки фотографии в личное сообщение.
        """
        return self.api.photos.getMessagesUploadServer()

    def get_photo_upload_server_url(self) -> str:
        """
        Возвращает адрес сервера для загрузки фотографии в личное сообщение.
        """
        return self.get_photo_upload_server()["upload_url"]

    @transform_exception
    def save_photo(self, photo: str, server: int, hash: str) -> dict:
        """
        Сохраняет фотографию после успешной загрузки на URI,
        полученный методом photos.getMessagesUploadServer.
        """
        response = self.api.photos.saveMessagesPhoto(
            photo=photo, server=server, hash=hash
        )
        return response[0]

    @transform_exception
    def get_docs_upload_server_url(self, peer_id: int, doc_type: str) -> str:
        """
        Возвращает адрес сервера для загрузки фотографии в личное сообщение.
        """
        if doc_type not in self.DOC_UPLOAD_TYPES:
            raise VkApiError(f"doc_type must be one of {self.DOC_UPLOAD_TYPES}")

        if doc_type == self.DOC:
            type_ = "doc"
        else:
            type_ = "audio_message"

        response = self.api.docs.getMessagesUploadServer(type=type_, peer_id=peer_id)
        return response["upload_url"]

    @transform_exception
    def save_doc(self, file: dict) -> dict:
        """
        Сохраняет фотографию после успешной загрузки на URI,
        полученный методом photos.getMessagesUploadServer.
        """
        response = self.api.docs.save(file=file)
        return response

    @transform_exception
    def save_video(self) -> dict:
        response = self.api.video.save(is_private=1, no_comments=1)
        return response

    def raise_limit_callback_servers(self, servers: List[CallbackServer]) -> None:
        """
        Проверяет лимит подключенные серверов к странице
        """
        if len(list(servers)) >= self.servers_limit:
            raise VkApiError(
                {
                    "error_msg": (
                        f"К странице VK нельзя добавлять больше "
                        f"{self.servers_limit} "
                        f"Callback-серверов."
                    )
                }
            )

    def get_deeplink(
        self, group_id: str, vk_mini_app_id: Optional[str] = None, **context: Any,
    ) -> str:
        """
        Формируется диплинк.
        """

        ctx = ",".join([f"{k}:{v}" for k, v in context.items()])

        if not vk_mini_app_id:
            # Для диплинков без миниаппы
            link = f"https://vk.com/write-{group_id}"
            if ctx:
                link += f"?ref={ctx}"
        else:
            # для диплинков с миниаппой
            # https://vk.com/app7156066_-168440122#a:1,b:2
            link = f"https://vk.com/app{vk_mini_app_id}_-{group_id}"
            if ctx:
                link += f"#{ctx}"
        return link


def guess_message_type(url: str) -> str:
    """
    Определить по URL, картинка это или документ.
    Facebook отличается от Telegram тем, что распознает GIF как картинку,
    в то время как Telegram считает  GIF документом

    (и анимация не работает, если слать анимированный GIF как картинку)
    """
    url_no_get_params = url.split("?")[0].lower()

    image_extensions = ["jpg", "jpeg", "png"]
    video_extensions = ["mp4", "avi", "mpeg"]
    audio_extensions = ["mp3", "ogg", "m4a", "opus"]
    if url_no_get_params.endswith(tuple(image_extensions)):
        return "image"
    if url_no_get_params.endswith(tuple(video_extensions)):
        return "video"
    if url_no_get_params.endswith(tuple(audio_extensions)):
        return "audio"
    return "document"


class VkontakteUploaderError(Exception):
    """
    Документы и файлы загружаются от имени пользователя, который подключил страницу.
    Если статус пользователя изменился на странице,
    и у него больше нет прав для загрузки,
    то мы получим ошибку 901, и интерпретируем ее, как подписчик заблокировал бота
    при отправке ему сообщения, что неверно, так как с подписчиком все нормально.
    Следует отличать такие ошибки и знать о них.
    """


def memoize(method):
    """
    Мемоизирует вызовы обернутого метода при помощи self.cache
    """

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):

        key = str(args) + str(kwargs)
        value = self.cache.get(key)
        if value:
            return value
        try:
            value = method(self, *args, **kwargs)
            self.cache.set(key, value)
            return value
        except exceptions.VkAPIError as e:
            tb = e.__traceback__
            raise VkApiError(error_data=e.error_data).with_traceback(tb) from None

    return wrapper


class VkontakteUploader:
    """
    Загрузчик файлов в ВК.
    Принимает файлы с Amazon AWS, грузит их ВК и ложит информацию о файле
    ВК в кеш. Если в кеше уже находится инфа о файле с урлом, то не грузим
    его второй раз
    """

    def __init__(
        self, vk_page_api: VKApi, vk_user_api: VKApi, cache: BaseCache
    ) -> None:
        self.cache = cache
        self.vk_page_api = vk_page_api
        self.vk_user_api = vk_user_api

    @memoize
    def get_attachment_id(self, attachment_url: str, vk_user_token: str) -> str:
        """
        Загружает файл на сервер ВК, получает ID загруженного файла и кеширует его.

        vk_user_token нужен для правильной мемоизации,
        чтобы участвовать в ключе кэша, на случай, если токен поменяется
        и старые закешированные ID вложений не будут отправляться с новым токеном
        """
        file_request = requests.get(attachment_url)
        content_type = file_request.headers.get("content-type")
        file_name = os.path.basename(urlparse(attachment_url).path)
        file_name, ext = os.path.splitext(file_name)
        if not ext and content_type:
            ext = mimetypes.guess_extension(content_type) or ""
        file_name = f"{file_name}{ext}"
        bytes_io = BytesIO(file_request.content)
        # Строим tuple, так как ВК требует файлы в формате
        # multipart/form-data. И чтобы они отправлялись в таком формате
        # надо передать в requests tuple имеющий информацию о названии файла
        # байтах файла и его типе
        file_tuple = (file_name, bytes_io.getvalue(), content_type)
        attachment_type = guess_message_type(attachment_url)
        try:
            if attachment_type == "image":
                attachment_id = self.upload_photo(file_tuple)
            elif attachment_type == "video":
                attachment_id = self.upload_video(file_tuple)
            elif attachment_type == "audio":
                # Грузим аудио, как аудиосообщение в ВК
                attachment_id = self.upload_document(file_tuple, self.vk_user_api.AUDIO)
            else:
                attachment_id = self.upload_document(file_tuple, self.vk_user_api.DOC)
        except VkApiError as e:
            tb = e.__traceback__
            raise VkontakteUploaderError(str(e)).with_traceback(tb) from None
        return attachment_id

    def upload_photo(self, file_tuple: Tuple[str, bytes, Optional[str]]) -> str:
        """
        Загружаем фотографию в ВК
        :param file_tuple: информация о файле
        """
        upload_url = self.vk_page_api.get_photo_upload_server_url()
        request = requests.post(upload_url, files={"photo": file_tuple})

        photo_upload_data = request.json()
        params = {
            "server": photo_upload_data["server"],
            "photo": photo_upload_data["photo"],
            "hash": photo_upload_data["hash"],
        }
        photo_data = self.vk_page_api.save_photo(**params)
        photo_id = "photo{0}_{1}_{2}".format(
            photo_data["owner_id"], photo_data["id"], photo_data["access_key"]
        )
        return photo_id

    def upload_video(self, file_tuple: Tuple[str, bytes, Optional[str]]) -> str:
        """
        Загружаем видео в ВК

        В текущее время не работает, так как грузить видео с помощью токенов
        сообществ нельзя
        :param file_tuple: информация о файле
        """
        upload_server_data = self.vk_user_api.save_video()
        upload_url = upload_server_data["upload_url"]
        request = requests.post(upload_url, files={"video_file": file_tuple})

        video_data = request.json()
        access_key = upload_server_data["access_key"]
        video_id = "video{0}_{1}_{2}".format(
            video_data["owner_id"], video_data["video_id"], access_key
        )
        return video_id

    def upload_document(
        self, file_tuple: Tuple[str, bytes, Optional[str]], doc_type: str
    ) -> str:
        """
        Загружаем документ в ВК
        :param file_tuple: информация о файле
        :param doc_type: тип документа (либо DOC — документ, либо AUDIO_MESSAGE
        — аудиосообщение)
        """
        if doc_type not in self.vk_page_api.DOC_UPLOAD_TYPES:
            raise Exception(
                f"doc_type must be one of {self.vk_page_api.DOC_UPLOAD_TYPES}"
            )
        upload_url = self.vk_page_api.get_docs_upload_server_url(
            doc_type=doc_type, peer_id=self.vk_user_api.user_id
        )
        request = requests.post(upload_url, files={"file": file_tuple})

        document_upload_data = request.json()
        params = {"file": document_upload_data["file"]}
        response = self.vk_page_api.save_doc(**params)
        if doc_type == self.vk_page_api.DOC:
            doc_data = response["doc"]
        else:
            doc_data = response["audio_message"]

        photo_id = "doc{0}_{1}".format(doc_data["owner_id"], doc_data["id"])
        return photo_id

    def __repr__(self):
        """
        для того, чтобы мемоизация работала. Иначе memoize будет смотреть на id объекта,
        он каждый раз будет разный, и смысла в мемоизации не будет
        """
        return self.__class__.__name__
