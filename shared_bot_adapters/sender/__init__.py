from dataclasses import dataclass
from typing import Optional, Type
from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration
from shared_bot_adapters.parsers.base_parser import AbstractTokens
from shared_bot_adapters.parsers.parse import VIBER, TELEGRAM, VKONTAKTE
from shared_bot_adapters.parsers.telegram import TelegramTokens
from shared_bot_adapters.parsers.viber import ViberTokens
from shared_bot_adapters.parsers.vkontakte import VkontakteTokens
from shared_bot_adapters.sender._base import BaseSender
from shared_bot_adapters.sender.adapters.telegram import TGApi
from shared_bot_adapters.sender.adapters.vkontakte import VKAppConfig, VKApi
from shared_bot_adapters.sender.telegram import TelegramSender
from shared_bot_adapters.sender.viber import ViberSender
from shared_bot_adapters.sender.vkontakte import VkontakteSender
from shared_bot_adapters.storage import BaseCache


@dataclass
class ViberSenderConfig:
    name: str
    avatar: str
    auth_token: str


@dataclass
class TelegramSenderConfig:
    token: str


@dataclass
class VkontakteSenderConfig:
    app_client_id: int
    app_client_secret: str
    app_callback_server_name: str
    group_id: int
    group_token: str
    owner_id: int
    owner_token: str


@dataclass
class SenderConfigs:
    viber: Optional[ViberSenderConfig] = None
    telegram: Optional[TelegramSenderConfig] = None
    vkontakte: Optional[VkontakteSenderConfig] = None


def get_vk_page_api(sender_config: VkontakteSenderConfig) -> VKApi:
    config = VKAppConfig(
        client_id=sender_config.app_client_id,
        client_secret=sender_config.app_client_secret,
        callback_server_name=sender_config.app_callback_server_name,
    )
    return VKApi(
        group_id=sender_config.group_id, token=sender_config.group_token, config=config
    )


def get_vk_owner_api(sender_config: VkontakteSenderConfig) -> VKApi:
    config = VKAppConfig(
        client_id=sender_config.app_client_id,
        client_secret=sender_config.app_client_secret,
        callback_server_name=sender_config.app_callback_server_name,
    )
    return VKApi(
        group_id=sender_config.owner_id, token=sender_config.owner_token, config=config
    )


def get_sender(
    configs: SenderConfigs,
    platform: str,
    chat_id: str,
    cache: BaseCache,
    viber_api_version: Optional[int],
) -> Optional[BaseSender]:

    platform = platform.lower()

    if platform == VIBER and configs.viber:

        assert (
            viber_api_version is not None
        ), "Please specify viber_api_version for sending to viber."

        return ViberSender(
            viber_client=Api(
                BotConfiguration(
                    name=configs.viber.name,
                    avatar=configs.viber.avatar,
                    auth_token=configs.viber.auth_token,
                )
            ),
            chat_id=chat_id,
            api_version=viber_api_version,
            cache=cache,
        )
    elif platform == TELEGRAM and configs.telegram:
        return TelegramSender(
            api=TGApi(token=configs.telegram.token), chat_id=chat_id, cache=cache,
        )

    elif platform == VKONTAKTE and configs.vkontakte:
        page_api = get_vk_page_api(configs.vkontakte)
        user_api = get_vk_owner_api(configs.vkontakte)
        return VkontakteSender(
            chat_id=chat_id, page_api=page_api, user_api=user_api, cache=cache
        )

    return None
