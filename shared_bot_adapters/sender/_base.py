import inspect
import logging
from abc import ABC, abstractmethod
from typing import Callable, Optional, Sequence, Type, List, Union

from ..exceptions import (
    SenderBadContentError,
    SenderBotIsBlockedError,
    SenderUserNotFoundError,
    SendRequestError,
)

from ..state_objects import BotState, TGQRButtonGroup
from ..state_objects.common import AbstractMessage, AbstractQRButtonGroup
from ..storage import BaseCache
from ..utils import flatten_irregular_list

ERROR_MSG = "Упс... ошибка. Попробуем заново?"


# словарь, в котором декоратор handler_for будет
# регистрировать все хендлеры
message_handlers = {}


def handler_for(*types: Type["AbstractMessage"]) -> Callable[[Callable], Callable]:
    """
    Декоратор, при помощи которого сендеры-наследники регистрируют методы
    отправки сообщений разных типов.
    Эти методы копятся в словаре BaseSender.message_handlers
    """

    def register(method):
        method_name = method.__name__

        # проверяем, что у всех хендлеров одинаковая сигнатура
        current_key_set = set(inspect.signature(method).parameters.keys())
        if current_key_set != {"self", "message", "qr_buttons"}:
            raise SyntaxError(
                f'Message handler "{method_name}" must have '
                f'"message" and "qr_buttons" parameters.'
            )

        for type_ in types:
            # Проверяем, что у нас нет нескольких хендлеров на один и тот же тип
            if type_ in message_handlers:
                raise Exception(f"Tried to duplicate message handler for {type_}")
            message_handlers[type_] = method
        return method

    return register


logger = logging.getLogger(__name__)


class BaseSender(ABC):
    """
    Базовый класс, от кторого наследуются все сендеры
    """

    def __init__(self, chat_id: str, cache: BaseCache):
        assert isinstance(cache, BaseCache), "cache must be an instance of BaseCache"
        self.chat_id = chat_id
        self.cache = cache

    @abstractmethod
    def _send_error(
        self, error_message: str, qr_buttons: Optional[AbstractQRButtonGroup]
    ) -> None:
        """
        Метод отправки ошибки, который должен быть переопределен
        в наследниках. Метод должен формировать текстовое сообщение
        и отправлять его
        """

    @abstractmethod
    def _send_only_qr_buttons(self, qr_buttons: AbstractQRButtonGroup) -> None:
        """
        Метод, отправки только QR-кнопок. Должен быть переопределен
        в наследниках. Если API каких-то мессенджеров не поддерживают
        отправку QR-кнопок, пусть просто вызывают NotImplementedError
        """

    def send_state(self, state: "BotState") -> List[int]:
        """
        Отправить сообщение разными методами
        в зависимости от наличия state.messages и state.qr_buttons.
        Вернуть список идентификаторов сообщений (если они есть).
        :param state: стейт для отправки
        """
        # пустые стейты (без сообщений и QR-кнопок) не отправляем
        if len(state.messages) == 0 and not state.qr_buttons:
            return []

        msg_ids: List[Optional[int]] = []

        messages = state.messages

        # Если сообщений нет - отправляем только QR-кнопки
        # методом self.send_only_qr_buttons
        if len(messages) == 0:
            try:
                if state.qr_buttons:
                    msg_id = self._send_only_qr_buttons(state.qr_buttons)
                    msg_ids.append(msg_id)
            except (SenderUserNotFoundError, SenderBotIsBlockedError):
                # если исключение связано с тем,
                # что юзер нас заблокировал или юзер не существует, -
                # не пытаемся отправить ему ошибку. Это бессмысленно.
                logger.info(f"Unable to send qr_buttons to {self.chat_id}",)
                raise
            except Exception as e:
                self._send_error(error_message=str(e), qr_buttons=None)
                raise
            return []

        all_but_last: Sequence[AbstractMessage]

        # Если сообщения таки есть: делим их
        # на последнее сообщение и все предыдущие
        if len(messages) == 1:
            last_message = messages[0]
            all_but_last = []
        else:
            last_message = messages[-1]
            all_but_last = messages[:-1]

        # Отправляем предыдущие сообщения без QR-кнопок...
        for message in all_but_last:
            msg_id = self._send_message(message, qr_buttons=None)
            msg_ids.append(msg_id)
        # ... а последнее сообщение - с QR-кнопками
        qr_buttons = state.qr_buttons

        msg_id = self._send_message(last_message, qr_buttons)
        msg_ids.append(msg_id)
        flattened = flatten_irregular_list(msg_ids)
        return [msg_id for msg_id in flattened if msg_id is not None]

    def _send_message(
        self, message: AbstractMessage, qr_buttons: Optional[AbstractQRButtonGroup]
    ) -> Optional[int]:
        """
        отправить сообщение и поймать возможные ошибки
        :param message: сообщение
        :param qr_buttons: кнопки
        """
        handler = message_handlers.get(message.__class__)
        if not handler:
            raise SenderBadContentError(
                f"Unable to find suitable handler "
                f"for sending {message.__class__.__name__}"
            )
        try:
            return handler(self, message, qr_buttons)
        except (SenderUserNotFoundError, SenderBotIsBlockedError, SendRequestError):
            # если исключение связано с тем,
            # что юзер нас заблокировал или юзер не существует,
            # или запрос не удался,
            # не пытаемся отправить ему ошибку. Это бессмысленно.
            logger.info(f"Unable to send message {message} to {self.chat_id}",)
            raise

        except Exception as e:
            # отправляем пользователю "Упс" и потом уже вызываем исключение.
            # Делать здесь сложную логику с получением ноды error_node не стоит,
            # т.к. иначе получаются очень сложно распутываемые макароны
            self._send_error(error_message=str(ERROR_MSG), qr_buttons=qr_buttons)
            raise
