from typing import Optional


class SenderBotIsBlockedError(Exception):
    """
    Ошибка, которая выдается при отправке сообщения,
    если пользователь заблокировал бота
    """


class SenderBadContentError(Exception):
    """
    Ошибка, которая выдается при отправке сообщения, если части контента,
    которые предполагается передать, несовместимы друг с другом.
    """


class SenderUserNotFoundError(Exception):
    """
    Ошибка, которая выдается, если пользователь, для которого предназаначалось
    сообщение, не найден в мессенджере по ID.
    """


class StateObjectError(Exception):
    """
    Ошибка, которая вызывается при некорректном формировании объектов
    для стейта: Message, TextMessage, FBCard и т.д.

    Сюда можно передать человекочитаемую ошибку message_for_frontend на русском,
     чтобы юзеры на фронте не пугались.
    """

    def __init__(
        self, message: str, message_for_frontend: Optional[str] = None
    ) -> None:
        self.message = message
        self.message_for_frontend = message_for_frontend


class BuilderError(Exception):
    """Ошибка в работе билдера сообщений"""


class SendRequestError(Exception):
    """
    Ошибка, когда API мессенджера когда во время отправки, произошла ошибка
    (превышен лимит запросов, не удалось достучаться до сервера...)
    """

    def __init__(
        self, retry_after: int, detail: str, retry_limit: Optional[int] = None
    ) -> None:
        """

        Args:
            retry_after: через сколько секунд нужно повторить.
            detail: описание ошибки
            retry_limit: ограничение на кол-во повторов
        """
        self.retry_after = retry_after
        self.detail = detail
        self.retry_limit = retry_limit
        super().__init__(retry_after, detail, retry_limit)
