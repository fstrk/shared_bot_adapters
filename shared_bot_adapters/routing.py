import json
import logging
from typing import Dict, Any, Tuple, Optional, Union, Callable

SCREEN_DICT: Dict[str, Callable] = {}

logger = logging.getLogger(__name__)


class ResolverError(Exception):
    pass


PythonObjKwargs = Dict[str, Optional[Union[str, int, bool]]]


class BotRouter:
    """
    Роутер, который нужно создать, чтобы декорировать вьюхи.
    Разные боты требуют разных роутеров. Это что-то вроде неймспейса.

    Пример декорирования вьюх:

    router1 = BotRouter('router1')

    @router1.screen
    def some_screen(a, b):
        ...


    Пример ссылок на вьюхи:

    data = router1.link(some_screen, a=1, b=2)

    """

    def __init__(self, name):
        self.name = name

    def func_name(self, func: Callable) -> str:
        return f"{self.name}/{func.__name__}"

    def screen(self, func):
        """
        Декоратор, которым нужно оборачивать экраны, генерящие стейт,
        чтобы внутри них работала функция create_func_call() для перелинковки друг на друга
        """
        func.key = self.func_name(func)

        if func.key in SCREEN_DICT:
            raise Exception(
                f"Function with name {func.key} is already registered in screen dict. "
                f"Create function with another name."
            )
        SCREEN_DICT[func.key] = func
        return func


def link(func: Callable, **kwargs: Any) -> str:
    """ взять функцию и аргументы, сохранить ее ключ (присвоенный декоратором screen) + аргументы в JSON-строке """
    if not hasattr(func, "key"):
        raise Exception(
            f"Function {func} is not decorated with @Router.screen "
            f"decorator and cannot be in a link."
        )

    obj = {"func": func.key, **kwargs}  # type: ignore
    return json.dumps(obj, ensure_ascii=False)


def resolve_link(json_string: str) -> Tuple[Callable, PythonObjKwargs]:
    """
    взять JSON-строку, извлечь имя функции и аргументы,
    вернуть их
    """
    try:
        kwargs = json.loads(json_string)
        func_name = kwargs.pop("func")
        func = SCREEN_DICT[func_name]
    except Exception as e:
        raise ResolverError(e)

    return func, kwargs
